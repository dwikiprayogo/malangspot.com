package com.example.dwikiprayogo.malangspot.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
//import com.example.dwikiprayogo.malangspot.utils.ImageLoader;
import com.example.dwikiprayogo.malangspot.adapter.AdapterKomentar;
import com.example.dwikiprayogo.malangspot.adapter.SpotAdapter;
import com.example.dwikiprayogo.malangspot.app.AppController;

import com.example.dwikiprayogo.malangspot.data.KomentarData;
import com.example.dwikiprayogo.malangspot.data.SpotData;
import com.google.ads.afma.nano.NanoAfmaSignals;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SpotDetail extends AppCompatActivity implements OnGestureListener,SwipeRefreshLayout.OnRefreshListener{

    GestureDetector gestureDetector;

    Toolbar toolbar;
    LinearLayout linearLayout;

    Integer id;
    Integer lenght_des;
    String judul,deskripsi,gambar,latitude,longitude,alamat, video_url;
    String[] data_gambar;
    long Spot_id;
    int successs;
    String Spot_kategori,Spot_nama,Spot_alamat,Spot_deskripsi,Spot_latitude,Spot_longitude,Spot_gambar;
    String url_detailSpot,url_imageSpot,url_list,jenis_komentar="video_spot", url_webview="http://malangspot.com/spot/jenis/Hotel";
    //ImageLoader imageLoader;
    TextView txt_kategori,txt_judul,
            txtAlert, txt_isi, txt_id, txt_latitude,
            txt_longitude, txt_alamat, txt_idKategori,url_video, nilai, txt_alert;
    ScrollView sclDetail;
    SwipeRefreshLayout swipe;
    ListView listkomen;
    WebView wb_detail;
    Button btn_foto,btn_video,btn_lokasi,btn_rekom;
    ProgressBar prgLoading;
    int IOConnect = 0;
    ImageView img_detail;
    private int offSet = 0;
    int no;
    //CustomListViewLands customListViewLands;
    //Bundle item;

    ProgressDialog pDialog;
    JSONParser json;
    JSONParser jsonParser = new JSONParser();
    String success;
    JSONArray jsonArray;
    JSONObject jsonObject;
    List<NameValuePair> list;

    List<KomentarData> komensList = new ArrayList<KomentarData>();
    AdapterKomentar adapter;

    String[] post_url_image;
    Integer length_json=0, value;

    ViewPager vp;
    ZoomControls zoom;
    Bundle item;

    ImageSliderAdapter sliderAdapter;

    private static final String TAG = SpotDetail.class.getSimpleName();

    public static final String TAG_ID 		= "id_tempat";
    public static final String TAG_JENIS 	= "jenis_tempat";
    public static final String TAG_NAMA 	= "nama_tempat";
    public static final String TAG_ALAMAT 	= "alamat";
    public static final String TAG_DESKRIPSI= "deskripsi";
    public static final String TAG_LATITUDE = "latitude";
    public static final String TAG_LONGITUDE = "longitude";
    public static final String TAG_VIDEO = "url";

    public static final String TAG_NO    = "no";
    public static final String TAG_NAME    = "nama";
    public static final String TAG_EMAIL   = "email";
    public static final String TAG_KOMEN   = "komen";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    //public static final String TAG_GAMBAR	= "gambar";

    //private static final String url_detail 	= Server.URL + "detail_news.php";
    String tag_json_obj = "json_obj_req";

    //private static String url = Server.link_image_detail_restaurant;

    Boolean cek_back=false;
    String url_image, id_tempat, jenis_tempat;

    FragmentManager fm;

    Dialog dialog;
    AlertDialog.Builder dialog_komen;
    LayoutInflater inflater;
    View dialogView;
    ImageView img;
    EditText tv_nama,tv_email,tv_komentar;

    private TrackGPS gps;
    double longitudes;
    double latitudes;

    boolean url_videoSpot=false;

    public static final String MyREF="mypres";
    public static final String Long="longKey";
    public static final String Lat="latKey";
    public static final String LongS="longSKey";
    public static final String LatS="latSKey";
    public static final String Tempat="TempatKey";
    public static final String Alamat="AlamatKey";
    public static final String Kategori="KategoriKey";

    Handler handler;
    Runnable runnable;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spot_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar_detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        linearLayout = (LinearLayout) findViewById(R.id.linierdetail);

        txt_judul = (TextView)findViewById(R.id.txt_judul);
        txt_kategori = (TextView) findViewById(R.id.txt_kategori);
        txt_isi = (TextView) findViewById(R.id.txt_isi);
        txtAlert = (TextView) findViewById(R.id.txtAlert);
        txt_id = (TextView) findViewById(R.id.txt_idTempat);
        txt_latitude = (TextView) findViewById(R.id.txt_latitude);
        txt_longitude = (TextView) findViewById(R.id.txt_longitude);
        txt_idKategori =(TextView) findViewById(R.id.id_kategori);
        txt_alamat = (TextView) findViewById(R.id.alamat);
        nilai = (TextView) findViewById(R.id.nilai);
        btn_foto = (Button)findViewById(R.id.btn_foto);
        btn_video = (Button)findViewById(R.id.btn_video);
        btn_lokasi = (Button)findViewById(R.id.btn_lokasi);
        wb_detail = (WebView) findViewById(R.id.detail_view);
        sclDetail = (ScrollView) findViewById(R.id.sv);
        btn_rekom = (Button) findViewById(R.id.btn_rekom);
        url_video = (TextView)findViewById(R.id.url_video);

        fm =getSupportFragmentManager();
        id_tempat = getIntent().getStringExtra(TAG_ID);
        jenis_tempat = getIntent().getStringExtra(TAG_JENIS);
        txt_kategori.setText(""+jenis_tempat);
        txt_id.setText(""+id_tempat);

        gestureDetector = new GestureDetector(SpotDetail.this, (OnGestureListener) SpotDetail.this);

        url_imageSpot = Server.url_image_spot+"id_tempat="+txt_id.getText().toString();
        url_list = Server.url_komen+"?jenis_komentar="+jenis_komentar+"&id_relasi="+id_tempat+"&offset=";
        url_detailSpot = Server.url_detaildataSpot;
        callDetailSpot(id_tempat);
        new load_image().execute();

        vp = (ViewPager)findViewById(R.id.viewPagerImage);

        btn_lokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gps = new TrackGPS(SpotDetail.this);

                if(gps.canGetLocation()){

                    longitudes = gps.getLongitude();
                    latitudes = gps .getLatitude();
                    sharedPreferences=getSharedPreferences(MyREF, Context.MODE_PRIVATE);
                    SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(SpotDetail.this);
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString(Long, String.valueOf(longitudes));
                    editor.putString(Lat, String.valueOf(latitudes));
                    editor.putString(LongS, String.valueOf(txt_longitude.getText().toString()));
                    editor.putString(LatS, String.valueOf(txt_latitude.getText().toString()));
                    editor.putString(Tempat, String.valueOf(txt_judul.getText().toString()));
                    editor.putString(Alamat, String.valueOf(txt_alamat.getText().toString()));
                    editor.commit();
                    startActivity(new Intent(SpotDetail.this,MapsActivity.class));
                    overridePendingTransition(R.anim.open_next, R.anim.close_next);

                    //Toast.makeText(getApplicationContext(),"Longitude:"+ Double.toString(longitudes)+"\nLatitude:"+ Double.toString(latitudes), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    gps.showSettingsAlert();
                }
            }
        });

        btn_rekom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences=getSharedPreferences(MyREF, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(SpotDetail.this);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString(Kategori, String.valueOf(txt_idKategori.getText().toString()));
                editor.commit();
                startActivity(new Intent(SpotDetail.this, RecommenActivity.class) );
                overridePendingTransition(R.anim.swip_next, R.anim.swipe_close);
            }
        });

        btn_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm("", "", "", "KIRIM");
            }
        });


        btn_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SpotDetail.this, ListVideo.class);
                intent.putExtra(TAG_ID, txt_id.getText().toString());
                startActivity(intent);

            }
        });

        wb_detail.loadUrl(url_webview);
        //setContentView(wb_detail);

    }

    // untuk menampilkan dialog from komen
    private void DialogForm(String nama_user, String email_user, String komentar, String button) {
        dialog_komen = new AlertDialog.Builder(SpotDetail.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.dialog_komentar, null);
        dialog_komen.setView(dialogView);
        dialog_komen.setCancelable(false);
        //dialog.setIcon(R.mipmap.ic_launcher);
        //dialog.setTitle("Form Biodata");

        img = (ImageView) dialogView.findViewById(R.id.imgLogo);
        tv_nama = (EditText) dialogView.findViewById(R.id.tv_nama);
        tv_email = (EditText) dialogView.findViewById(R.id.tv_email);
        tv_komentar = (EditText) dialogView.findViewById(R.id.tv_komentar);
        txt_alert = (TextView) dialogView.findViewById(R.id.txtAlert);

        swipe = (SwipeRefreshLayout) dialogView.findViewById(R.id.swipeToRefresh);
        listkomen = (ListView) dialogView.findViewById(R.id.listkomen);

        adapter = new AdapterKomentar(SpotDetail.this, komensList);
        listkomen.setAdapter(adapter);


        swipe.setRefreshing(true);
        swipe.setOnRefreshListener(this);

        komensList.clear();
        loadKomen(0);
        adapter.notifyDataSetChanged();

        //txt_id      = (EditText) dialogView.findViewById(R.id.txt_id);
        //txt_nama    = (EditText) dialogView.findViewById(R.id.txt_nama);
        //txt_alamat  = (EditText) dialogView.findViewById(R.id.txt_alamat);

        /*if (!idx.isEmpty()){
            txt_id.setText(idx);
            txt_nama.setText(namax);
            txt_alamat.setText(alamatx);
        } else {
            kosong();
        }*/

        dialog_komen.setPositiveButton(button, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                /*id      = txt_id.getText().toString();
                nama    = txt_nama.getText().toString();
                alamat  = txt_alamat.getText().toString();*/

                simpan();
                //dialog.dismiss();
            }
        });

        dialog_komen.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //kosong();
            }
        });

        listkomen.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }

            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                    swipe.setRefreshing(true);

                    handler = new Handler();
                    runnable = new Runnable() {
                        public void run() {
                            loadKomen(offSet);
                            //txt_alert.setVisibility(View.GONE);
                        }
                    };
                    handler.postDelayed(runnable, 1000);

                }
            }

        });

        dialog_komen.show();
    }

    @Override
    public void onRefresh() {
        komensList.clear();
        adapter.notifyDataSetChanged();
        loadKomen(0);
    }

    @Override
    public void onBackPressed() {
        //overridePendingTransition(R.anim.open_main, R.anim.close_next);
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
        //customListViewLands.imageLoader.clearCache();
        //clearData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                //customListViewLands.imageLoader.clearCache();
                //clearData();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void callDetailSpot(final String id_tempat){
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading On Progress");
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        //swipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.POST, url_detailSpot, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response " + response.toString());
                //swipe.setRefreshing(false);

                try {
                    JSONObject obj = new JSONObject(response);

                    String id_tempat    = obj.getString(TAG_JENIS);
                    String nama_tempat   = obj.getString(TAG_NAMA);
                    String deskripsi      = obj.getString(TAG_DESKRIPSI);
                    String latitude     = obj.getString(TAG_LATITUDE);
                    String longitude     = obj.getString(TAG_LONGITUDE);
                    String alamat = obj.getString(TAG_ALAMAT);
                    String url = obj.getString(TAG_VIDEO);
                    //String Isi      = obj.getString(TAG_ISI);

                    txt_judul.setText(nama_tempat);
                    txt_idKategori.setText(id_tempat);
                    //txt_kategori.setText(jenis_tempat);
                    txt_isi.setText(Html.fromHtml(deskripsi));
                    //wb_detail.loadData("<p style=\"text-align: justify\">" + item.getString("deskripsi") + "</p>", "text/html" , "UTF-8");
                    txt_latitude.setText(latitude);
                    txt_longitude.setText(longitude);
                    nilai.setText(""+url);
                    txt_alamat.setText(alamat);
                    //txt_alamat.setText(url);
                    /*if(url=="0"){
                        url_videoSpot=true;
                        //nilai.setText("0");
                        //url_videoSpot=true;
                        //url_videoSpot=true;
                    }else{
                        //txt_alamat.setText("");
                        //nilai.setText(url);
                        //url_videoSpot=false;
                    }

                    //url_video.setText(url);

                    /*if (obj.getString(TAG_GAMBAR)!=""){
                        //thumb_image.setImageUrl(Gambar, imageLoader);
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Detail Spot Error: " + error.getMessage());
                Toast.makeText(SpotDetail.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //swipe.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_tempat", id_tempat);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        //linearLayout.setVisibility(View.VISIBLE);
        progressDialog.dismiss();
    }


    public class load_image extends AsyncTask<String, String, String>
    {


        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SpotDetail.this);
            pDialog.setIndeterminate(false);

        }

        @Override
        protected String doInBackground(String... arg0) {


            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(url_imageSpot);

            try {
                success = json.getString("success");

                Log.e("error", "nilai sukses=" + success);

                JSONArray hasil = json.getJSONArray("data_gambar");

                if (success.equals("1")) {

                    data_gambar = new String[hasil.length()];
                    for (int i = 0; i < hasil.length(); i++) {

                        JSONObject c = hasil.getJSONObject(i);
                        data_gambar[i] = c.getString("gambar".trim());
                        //session.get_data(jumlah_hit,jumlah_tim,jumlah_kal);
                        Log.e("ok", " ambil data");
                    }
                } else {
                    Log.e("error", "tidak bisa ambil data 0");
                }

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("error", "tidak bisa ambil data 1");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (success.equals("1")) {
                linearLayout.setVisibility(View.VISIBLE);
                //Toast.makeText(getApplicationContext(), "Data sukses masuk broo ...", Toast.LENGTH_SHORT).show();
                sliderAdapter = new ImageSliderAdapter(SpotDetail.this,data_gambar);
                vp.setAdapter(sliderAdapter);
            } else {
                Toast.makeText(getApplicationContext(), "Sorry, failed to response ...", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void loadKomen(final int page){

        swipe.setRefreshing(true);

        // Creating volley request obj
        final JsonArrayRequest arrReq = new JsonArrayRequest(url_list + page,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    KomentarData komen = new KomentarData();

                                    no = obj.getInt(TAG_NO);

                                    komen.setNama(obj.getString(TAG_NAME));
                                    komen.setEmail(obj.getString(TAG_EMAIL));

                                    komen.setKomen(obj.getString(TAG_KOMEN));
                                    //spot.setJenis_tempat(obj.getString(TAG_JENIS));

                                    // adding news to news array
                                    komensList.add(komen);

                                    if (komensList.size()<1){
                                        txt_alert.setVisibility(View.VISIBLE);
                                    }else {
                                        swipe.setVisibility(View.VISIBLE);
                                        listkomen.setVisibility(View.VISIBLE);
                                        txt_alert.setVisibility(View.GONE);
                                    }

                                    if (no > offSet) {
                                        offSet = no;
                                    }

                                    Log.d(TAG, "offSet " + offSet);

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }

                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                adapter.notifyDataSetChanged();
                            }
                        }
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);

    }

    // fungsi untuk menyimpan atau update
    private void simpan() {
        String url;
        // jika id kosong maka simpan, jika id ada nilainya maka update
            url = Server.url_insert_komen;

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    successs = jObj.getInt(TAG_SUCCESS);

                    // Cek error node pada json
                    if (successs == 1) {
                        Log.d("Add/update", jObj.toString());

                        komensList.clear();
                        adapter.notifyDataSetChanged();
                        loadKomen(0);
                        //callVolley();
                        //kosong();

                        Toast.makeText(SpotDetail.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        adapter.notifyDataSetChanged();

                    } else {
                        Toast.makeText(SpotDetail.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(SpotDetail.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                // jika id kosong maka simpan, jika id ada nilainya maka update
                params.put("id_relasi", id_tempat);
                params.put("nama", tv_nama.getText().toString());
                params.put("email", tv_email.getText().toString());
                params.put("komen", tv_komentar.getText().toString());
                params.put("jenis_komentar", jenis_komentar);

                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if(e1.getY() - e2.getY() > 10){
            startActivity(new Intent(SpotDetail.this, RecommenActivity.class));
            overridePendingTransition(R.anim.swip_next, R.anim.swipe_close);

            return true;
        }
        else {

            return true ;
        }
    }

    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {

        return gestureDetector.onTouchEvent(motionEvent);
    }

    @Override
    public boolean onFling(MotionEvent motionEvent1, MotionEvent motionEvent2, float X, float Y) {

        return false;
    }
}
