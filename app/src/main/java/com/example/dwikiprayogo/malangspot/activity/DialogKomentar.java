package com.example.dwikiprayogo.malangspot.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.dwikiprayogo.malangspot.R;

/**
 * Created by Kuhaku on 22/03/2017.
 */

public class DialogKomentar extends DialogFragment {

    Button btn;
    ImageView img;
    EditText tv_nama,tv_email,tv_komentar;
    String nama,email,komentar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_komentar,container,false);
        getDialog().setTitle("Komentar");
        //btn = (Button)view.findViewById(R.id.btn_komentar);
        img = (ImageView) view.findViewById(R.id.imgLogo);
        tv_nama = (EditText) view.findViewById(R.id.tv_nama);
        tv_email = (EditText) view.findViewById(R.id.tv_email);
        tv_komentar = (EditText)view.findViewById(R.id.tv_komentar);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nama = tv_nama.getText().toString();
                email = tv_email.getText().toString();
                komentar = tv_komentar.getText().toString();
                if(nama != "" || nama !=null || email != "" || email !=null || komentar != "" || komentar !=null ){
                    Toast.makeText(getActivity(),"nama :"+nama+" komentar :"+komentar, Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getActivity(),"Data harus lengkap", Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }


}
