package com.example.dwikiprayogo.malangspot.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.adapter.SpotAdapter;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.SpotData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RecommenActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    TextView txt_kategori,txt_alert;
    ListView list;

    SwipeRefreshLayout swipe;
    List<SpotData> spotsList = new ArrayList<SpotData>();

    private static final String TAG = SpotActivity.class.getSimpleName();

    //private static String url_search = Server.url_website + "cari_data.php?keyword=";

    private int offSet = 0;
    private String Keyword, url_search, url_list, jenis_tempat;

    int no, kategori_id;

    SpotAdapter adapter;

    public static final String TAG_NO       = "no";
    public static final String TAG_ID       = "id_tempat";
    public static final String TAG_JENIS    = "jenis_tempat";
    public static final String TAG_NAMA      = "nama_tempat";
    public static final String TAG_ALAMAT   = "alamat";
    public static final String TAG_GAMBAR   = "gambar";

    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_event);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        list = (ListView) findViewById(R.id.listspot);
        spotsList.clear();
        txt_kategori = (TextView) findViewById(R.id.kategori_recom);
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(RecommenActivity.this);
        String id_jenis= sharedPreferences.getString(SpotDetail.Kategori, null);
        txt_kategori.setText(""+id_jenis);
        txt_alert = (TextView) findViewById(R.id.txtAlert);
        url_list   = Server.url_spotlain+"?jenis_tempat="+txt_kategori.getText().toString()+"&offset=";
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        //getSupportActionBar().setHomeAsUpIndicator(upArrow);
        adapter = new SpotAdapter(RecommenActivity.this, spotsList);
        list.setAdapter(adapter);

        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           spotsList.clear();
                           adapter.notifyDataSetChanged();
                           loadSpot(0);
                           list.setVisibility(View.VISIBLE);
                           txt_alert.setVisibility(View.GONE);
                       }
                   }
        );

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(RecommenActivity.this, SpotDetail.class);
                intent.putExtra(TAG_ID, spotsList.get(position).getId_tempat());
                intent.putExtra(TAG_JENIS, spotsList.get(position).getJenis_tempat());
                startActivity(intent);
                overridePendingTransition(R.anim.open_next, R.anim.close_next);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(RecommenActivity.this, SpotDetail.class));
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }

    private void loadSpot(int page){

        swipe.setRefreshing(true);

        // Creating volley request obj
        JsonArrayRequest arrReq = new JsonArrayRequest(url_list + page,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    SpotData spot = new SpotData();

                                    no = obj.getInt(TAG_NO);

                                    spot.setId_tempat(obj.getString(TAG_ID));
                                    spot.setNama_tempat(obj.getString(TAG_NAMA));

                                    if (obj.getString(TAG_GAMBAR) != "") {
                                        spot.setGambar(obj.getString(TAG_GAMBAR));
                                    }

                                    spot.setAlamat(obj.getString(TAG_ALAMAT));
                                    spot.setJenis_tempat(obj.getString(TAG_JENIS));

                                    // adding news to news array
                                    spotsList.add(spot);

                                    /*if (no > offSet)
                                        offSet = no;*/

                                    Log.d(TAG, "offSet " + offSet);

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }

                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                adapter.notifyDataSetChanged();
                            }
                        }
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
    }

    @Override
    public void onRefresh() {
        spotsList.clear();
        adapter.notifyDataSetChanged();
        loadSpot(0);
    }
}
