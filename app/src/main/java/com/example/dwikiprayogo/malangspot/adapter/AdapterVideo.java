package com.example.dwikiprayogo.malangspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.SpotData;
import com.example.dwikiprayogo.malangspot.data.VideoData;

import java.util.List;

/**
 * Created by DWIKI PRAYOGO on 25/04/2017.
 */

public class AdapterVideo extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<VideoData> videosItems;
    ImageLoader imageLoader;

    public AdapterVideo(Activity act, List<VideoData> videosItems) {
        this.activity = act;
        this.videosItems = videosItems;
        //imageLoader = new ImageLoader(act);
    }

    @Override
    public int getCount() {
        return videosItems.size();
    }

    @Override
    public Object getItem(int location) {
        return videosItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.custom_list_video, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        //TextView txtNo = (TextView) convertView.findViewById(R.id.txt_no);
        TextView txt_id = (TextView) convertView.findViewById(R.id.id_video);
        //TextView txtKategori = (TextView) convertView.findViewById(R.id.txt_deskripsi);
        //TextView txtAlamat = (TextView)  convertView.findViewById(R.id.txt_alamat);
        NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.video_gambar);

        VideoData videos = videosItems.get(position);

        thumbNail.setImageUrl("http://img.youtube.com/vi/"+videos.getUrl_video()+"/0.jpg", imageLoader);
        txt_id.setText(videos.getUrl());
        //txtNo.setText(spots.getNo());
        //txtTextJudul.setText(spots.getNama_tempat());
        //txtKategori.setText(Html.fromHtml(spots.getDeskripsi()));
        //txtAlamat.setText(Html.fromHtml(spots.getAlamat()));

        return convertView;
    }

}
