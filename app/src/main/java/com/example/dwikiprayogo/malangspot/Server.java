package com.example.dwikiprayogo.malangspot;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Dwiki Prayogo on 15/03/2017.
 */

public class Server {
    //public static final String link_restaurant="http://192.168.43.15/apimalangspot/ambildata.php";
    //public static final String link_image_detail_restaurant="http://192.168.43.15/apimalangspot/ambil_gambar.php";

    public static final String DEVELOPER_KEY = "AIzaSyA60BDL4qE5zPlej1Wq1jcrpcpaD5hl1us";

    // API URL configuration
    public static String ip="http://192.168.43.15/";
    public static String url_alldata = ip+"/apimalangspot/ambil_dataSpot.php";
    public static String url_image_spot= ip+"/apimalangspot/ambil_gambarSpot.php?";
    public static String url_image_artikel= ip+"/apimalangspot/ambil_gambarArtikel.php?";
    public static String url_imageTalent= ip+"/apimalangspot/ambil_gambarTalent.php?";
    public static String url_imageEvent= ip+"/apimalangspot/ambil_gambarEvent.php?";
    public static String url_image = ip+"/apimalangspot/ambil_gambar.php/";
    public static String url_website = ip+"/apimalangspot/data_spot.php/";
    public static String url_insert_komen = ip+"/apimalangspot/insert_komen.php/";
    public static String url_talent = ip+"/apimalangspot/data_talent.php/";
    public static String url_komen = ip+"/apimalangspot/data_komen.php/";
    public static String url_spotlain = ip+"/apimalangspot/data_rekom.php/";
    public static String url_eventlain = ip+"/apimalangspot/data_rekomEvent.php/";
    public static String url_relasi_artikel = ip+"/apimalangspot/data_relasiArtikel.php/";
    public static String url_website_artikel = ip+"/apimalangspot/data_artikel.php/";
    public static String url_website_video = ip+"/apimalangspot/ambil_dataVideo.php/";
    public static String url_event_video = ip+"/apimalangspot/ambil_eventVideo.php/";
    public static String url_image_video = "http://img.youtube.com/vi/";
    public static String url_imagelist_artikel = ip+"/apimalangspot/images/artikel/small/";
    public static String url_imagelist_event = ip+"/apimalangspot/images/event/small/";
    public static String url_imagedetail_event = ip+"/apimalangspot/images/event/large/";
    public static String url_imagedetail_artikel = ip+"/apimalangspot/images/artikel/large/";
    public static String url_imagedetail_talent = ip+"/apimalangspot/images/talent/large/";
    public static String url_image_talent = ip+"/apimalangspot/images/talent/small/";
    public static String url_search = ip+"/apimalangspot/cari_data.php/";
    public static String url_search_artikel = ip+"/apimalangspot/cari_dataArtikel.php/";
    public static String url_search_event = ip+"/apimalangspot/cari_dataEvent.php/";
    public static String url_search_talent = ip+"/apimalangspot/cari_dataTalent.php/";
    public static String url_detaildataSpot = ip+"/apimalangspot/data_detail.php/";
    public static String url_detaildataArtikel = ip+"/apimalangspot/data_detailArtikel.php/";
    public static String url_detailfooterArtikel = ip+"/apimalangspot/data_footerArtikel.php/";
    public static String url_detaildataTalent = ip+"/apimalangspot/data_detailTalent.php/";
    public static String url_detaildataEvent = ip+"/apimalangspot/data_detailEvent.php/";
    public static String url_dataEvent = ip+"/apimalangspot/data_event.php/";
    public static String url_detaildata = ip+"/apimalangspot/images/tempat/small/";
    public static final String url_dataSpot = ip+"/apimalangspot/data_spot.php";

    // change this access similar with accesskey in admin panel for security reason
    public static String AccessKey = "12345";

    // database path configuration
    public static String DBPath = "/data/data/com.example.strixpc.sbdtshop/databases/";

    // method to check internet connection
    public static boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // method to handle images from server
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
}
