package com.example.dwikiprayogo.malangspot.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.adapter.EventAdapter;
import com.example.dwikiprayogo.malangspot.adapter.SpotAdapter;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.EventData;
import com.example.dwikiprayogo.malangspot.data.SpotData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RecomenEvent extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    Toolbar toolbar;
    EditText etsearch;
    TextView txt_kategori,txt_alert;
    ImageView img_x;
    ListView list;
    boolean value_anim=true;
    SwipeRefreshLayout swipe;
    List<EventData> eventsList = new ArrayList<EventData>();

    private static final String TAG = ListArtikel.class.getSimpleName();


    private int offSet = 0;
    private String Keyword, url_search, url_list, jenis_tempat;

    int no, kategori_id;

    EventAdapter adapter;

    public static final String TAG_NO       = "no";
    public static final String TAG_ID       = "id_event";
    public static final String TAG_JUDUL    = "nama_event";
    public static final String TAG_TGL      = "tgl_event";
    public static final String TAG_LOKASI   = "lokasi";
    public static final String TAG_ISI      = "deskripsi_event";
    public static final String TAG_GAMBAR   = "gambar";

    Handler handler;
    Runnable runnable;

    Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recomen_event);

        //etsearch = (EditText) findViewById(R.id.edtKeyword);
        //img_x = (ImageView) findViewById(R.id.xcross_img);
        //img_x.setVisibility(View.GONE);

        url_list   = Server.url_eventlain+"?offset=";

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        list = (ListView) findViewById(R.id.listspot);
        eventsList.clear();
        //etsearch = (EditText) findViewById(R.id.edtKeyword);
        txt_alert = (TextView) findViewById(R.id.txtAlert);

        toolbar = (Toolbar) findViewById(R.id.toolbar_event);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        //getSupportActionBar().setHomeAsUpIndicator(upArrow);

        adapter = new EventAdapter(RecomenEvent.this, eventsList);
        list.setAdapter(adapter);

        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           eventsList.clear();
                           adapter.notifyDataSetChanged();
                           loadEvent(0);
                           list.setVisibility(View.VISIBLE);
                           txt_alert.setVisibility(View.GONE);
                       }
                   }
        );

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(RecomenEvent.this, EventDetail.class);
                intent.putExtra(TAG_ID, eventsList.get(position).getId_event());
                intent.putExtra(TAG_JUDUL, eventsList.get(position).getJudul_event());
                intent.putExtra(TAG_TGL, eventsList.get(position).getTanggal_event());
                startActivity(intent);
                overridePendingTransition(R.anim.open_next, R.anim.close_next);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(RecommenActivity.this, SpotDetail.class));
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }

    private void loadEvent(int page){

        swipe.setRefreshing(true);

        // Creating volley request obj
        JsonArrayRequest arrReq = new JsonArrayRequest(url_list + page,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    EventData event = new EventData();

                                    no = obj.getInt(TAG_NO);

                                    event.setId_event(obj.getString(TAG_ID));
                                    event.setJudul_event(obj.getString(TAG_JUDUL));

                                    if (obj.getString(TAG_GAMBAR) != "") {
                                        event.setGambar_event(obj.getString(TAG_GAMBAR));
                                    }

                                    event.setTanggal_event(obj.getString(TAG_TGL));
                                    event.setLokasi_event(obj.getString(TAG_LOKASI));
                                    //event.setIsi_event(obj.getString(TAG_ISI));

                                    // adding news to news array

                                    eventsList.add(event);

                                    /*if (no > offSet)
                                        offSet = no;*/

                                    Log.d(TAG, "offSet " + offSet);

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }

                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                adapter.notifyDataSetChanged();
                            }
                        }
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
    }

    @Override
    public void onRefresh() {
        eventsList.clear();
        adapter.notifyDataSetChanged();
        loadEvent(0);
    }
}
