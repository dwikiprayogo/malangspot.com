package com.example.dwikiprayogo.malangspot.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.fragments.SpotFragment;

public class SplashActivity extends Activity {
    private final int splash =9000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.fade_in,R.anim.fadein);
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = null;

                mainIntent = new Intent(SplashActivity.this, HomeActivity.class);

                SplashActivity.this.startActivity(mainIntent);
                overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                SplashActivity.this.finish();
            }
        }, splash);

    }
}
