package com.example.dwikiprayogo.malangspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.SpotData;

import java.util.List;

/**
 * Created by DWIKI PRAYOGO on 19/04/2017.
 */

public class AdapterArtikelDet extends BaseAdapter{

    private Activity activity;
    private LayoutInflater inflater;
    private List<SpotData> spotsItems;
    ImageLoader imageLoader;

    public AdapterArtikelDet(Activity act, List<SpotData> spotsItems) {
        this.activity = act;
        this.spotsItems = spotsItems;
        //imageLoader = new ImageLoader(act);
    }

    @Override
    public int getCount() {
        return spotsItems.size();
    }

    @Override
    public Object getItem(int location) {
        return spotsItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.custom_listartikeldet, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        TextView txtNo = (TextView) convertView.findViewById(R.id.txt_no);
        TextView txtTextJudul = (TextView) convertView.findViewById(R.id.txt_nama);
        TextView txtKategori = (TextView) convertView.findViewById(R.id.txt_deskripsi);
        //TextView txtAlamat = (TextView)  convertView.findViewById(R.id.txt_alamat);
        //NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.spot_gambar);

        SpotData spots = spotsItems.get(position);

        //thumbNail.setImageUrl(Server.url_detaildata+spots.getGambar(), imageLoader);
        txtNo.setText(spots.getNo());
        txtTextJudul.setText(spots.getNama_tempat());
        txtKategori.setText(Html.fromHtml(spots.getDeskripsi()));
        //txtAlamat.setText(Html.fromHtml(spots.getAlamat()));

        return convertView;
    }


}
