package com.example.dwikiprayogo.malangspot.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.dwikiprayogo.malangspot.Server;
import com.squareup.picasso.Picasso;

/**
 * Created by Dwiki Prayogo on 23/03/2017.
 */

public class ImageSliderAdapter extends PagerAdapter {
    Context mContext;
    String[] url;
    Bitmap bmp;

    ImageSliderAdapter(Context context, String[] url) {
        this.mContext = context;
        this.url = url;
    }
    @Override
    public int getCount() {
        return url.length;
    }

    @Override
    public boolean isViewFromObject(View v, Object obj) {
        return v == ((ImageView) obj);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        //imageView.setScaleY(SubsamplingScaleImageView.SCALE_TYPE_CENTER_CROP);
        Picasso.with(container.getContext())
                .load(Server.url_detaildata+url[i])
                .into(imageView); //letakan gambar yang telah diload kedalam img*/
         //mImageView.setImageBitmap(bmp);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }
    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        ((ViewPager) container).removeView((ImageView) obj);
    }



}
