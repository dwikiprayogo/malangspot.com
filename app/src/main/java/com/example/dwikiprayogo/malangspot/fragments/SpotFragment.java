package com.example.dwikiprayogo.malangspot.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.activity.AdapterGridViewSpot;
import com.example.dwikiprayogo.malangspot.activity.GridViewItemSpot;
import com.example.dwikiprayogo.malangspot.activity.SpotActivity;

import java.util.ArrayList;

/**
 * Created by DWIKI PRAYOGO on 09/03/2017.
 */

public class SpotFragment extends Fragment implements OnItemClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    public static final String artikel_pref_name = "SpotPref" ;
    public static final String kategori_spot = "nameKey";

    GridView gridview;
    boolean value=true;
    AdapterGridViewSpot gridviewAdapter;
    ArrayList<GridViewItemSpot> data = new ArrayList<GridViewItemSpot>();

    SharedPreferences sharedpreferences;

    public SpotFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_spot, container, false);

        gridview = (GridView) v.findViewById(R.id.gridView2);
        gridview.setOnItemClickListener(this);

        if(value==true){
            data.add(new GridViewItemSpot(getResources().getString(R.string.menu_semua), getResources().getDrawable(R.drawable.ic_semua)));
            data.add(new GridViewItemSpot(getResources().getString(R.string.menu_cafe), getResources().getDrawable(R.drawable.ic_cafe)));
            data.add(new GridViewItemSpot(getResources().getString(R.string.menu_hotel), getResources().getDrawable(R.drawable.ic_hotel)));
            data.add(new GridViewItemSpot(getResources().getString(R.string.menu_resto),getResources().getDrawable(R.drawable.ic_restoran)));
            data.add(new GridViewItemSpot(getResources().getString(R.string.menu_guest),getResources().getDrawable(R.drawable.ic_guest)));
            data.add(new GridViewItemSpot(getResources().getString(R.string.menu_wisata),getResources().getDrawable(R.drawable.ic_wisata)));
        }else{
            //do nothing
        }

        setDataAdapter();
        return v;
    }

    private void setDataAdapter() {
        gridviewAdapter = new AdapterGridViewSpot(getActivity(), R.layout.fragment_adapter_spot, data);
        gridview.setAdapter(gridviewAdapter);
        value=false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (position==0){
              startActivity(new Intent(getActivity(), SpotActivity.class));
            getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
            sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(kategori_spot, "0");
            editor.commit();
        }
        else if (position==1){
            startActivity(new Intent(getActivity(), SpotActivity.class));
            getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
            sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(kategori_spot, "1");
            editor.commit();

        }
        else if (position==2){
            startActivity(new Intent(getActivity(), SpotActivity.class));
            getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
            sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(kategori_spot, "3");
            editor.commit();

        }
        else if (position==3){
            startActivity(new Intent(getActivity(), SpotActivity.class));
            getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
            sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(kategori_spot, "2");
            editor.commit();

        }else if (position==4){

            startActivity(new Intent(getActivity(), SpotActivity.class));
            getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
            sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(kategori_spot, "4");
            editor.commit();

        }else if (position==5){

            startActivity(new Intent(getActivity(), SpotActivity.class));
            getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
            sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(kategori_spot, "5");
            editor.commit();

        }

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
