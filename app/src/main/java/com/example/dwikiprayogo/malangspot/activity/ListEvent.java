package com.example.dwikiprayogo.malangspot.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.adapter.ArtikelAdapter;
import com.example.dwikiprayogo.malangspot.adapter.EventAdapter;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.ArtikelData;
import com.example.dwikiprayogo.malangspot.data.EventData;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ListEvent extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    Toolbar toolbar;
    EditText etsearch;
    TextView txt_kategori,txt_alert;
    ImageView img_x;
    ListView list;
    boolean value_anim=true;
    SwipeRefreshLayout swipe;
    List<EventData> eventsList = new ArrayList<EventData>();

    private static final String TAG = ListArtikel.class.getSimpleName();


    private int offSet = 0;
    private String Keyword, url_search, url_list, jenis_tempat;

    int no, kategori_id;

    EventAdapter adapter;

    public static final String TAG_NO       = "no";
    public static final String TAG_ID       = "id_event";
    public static final String TAG_JUDUL    = "nama_event";
    public static final String TAG_TGL      = "tgl_event";
    public static final String TAG_LOKASI   = "lokasi";
    public static final String TAG_ISI      = "deskripsi_event";
    public static final String TAG_GAMBAR   = "gambar";

    Handler handler;
    Runnable runnable;

    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_event);

        etsearch = (EditText) findViewById(R.id.edtKeyword);
        img_x = (ImageView) findViewById(R.id.xcross_img);
        img_x.setVisibility(View.GONE);

        url_search = Server.url_search_event+"?accesskey="+Server.AccessKey;
        url_list   = Server.url_dataEvent+"?offset=";

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        list = (ListView) findViewById(R.id.listspot);
        eventsList.clear();
        etsearch = (EditText) findViewById(R.id.edtKeyword);
        txt_alert = (TextView) findViewById(R.id.txtAlert);

        toolbar = (Toolbar) findViewById(R.id.toolbar_event);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        etsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (value_anim==true){
                    TranslateAnimation animate = new TranslateAnimation(img_x.getWidth(),0,0,0);
                    animate.setDuration(500);
                    animate.setFillAfter(true);
                    img_x.startAnimation(animate);
                    img_x.setVisibility(View.VISIBLE);
                    value_anim=false;
                    loadEvent(offSet);
                }else{
                    //do nothing
                    try {
                        Keyword = URLEncoder.encode(etsearch.getText().toString(), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    swipe.setRefreshing(true);
                    eventsList.clear();
                    adapter.notifyDataSetChanged();
                    url_search += "&keyword="+Keyword;
                    //IOConnect = 0;
                    list.invalidateViews();
                    new getDataSearch().execute();
                    txt_alert.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        img_x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etsearch.getText().clear();
                TranslateAnimation animate = new TranslateAnimation(0,img_x.getWidth(),0,0);
                animate.setDuration(500);
                animate.setFillAfter(true);
                img_x.startAnimation(animate);
                img_x.setVisibility(View.GONE);
                value_anim=true;
                swipe.setRefreshing(true);
                handler = new Handler();

                runnable = new Runnable() {
                    public void run() {
                        eventsList.clear();
                        adapter.notifyDataSetChanged();
                        loadEvent(0);
                        list.setVisibility(View.VISIBLE);
                        txt_alert.setVisibility(View.GONE);
                    }
                };

                handler.postDelayed(runnable, 1000);
            }
        });

        adapter = new EventAdapter(ListEvent.this, eventsList);
        list.setAdapter(adapter);

        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           eventsList.clear();
                           adapter.notifyDataSetChanged();
                           loadEvent(0);
                           list.setVisibility(View.VISIBLE);
                           txt_alert.setVisibility(View.GONE);
                       }
                   }
        );

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(ListEvent.this, EventDetail.class);
                intent.putExtra(TAG_ID, eventsList.get(position).getId_event());
                intent.putExtra(TAG_JUDUL, eventsList.get(position).getJudul_event());
                intent.putExtra(TAG_TGL, eventsList.get(position).getTanggal_event());
                startActivity(intent);
                overridePendingTransition(R.anim.open_next, R.anim.close_next);
            }
        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }

            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                    swipe.setRefreshing(true);
                    handler = new Handler();

                    runnable = new Runnable() {
                        public void run() {
                            loadEvent(offSet);
                            txt_alert.setVisibility(View.GONE);
                        }
                    };

                    handler.postDelayed(runnable, 1000);
                }
            }

        });

        Button btn_next = new Button(this);
        btn_next.setBackgroundResource(R.color.colorMaxTransparant);
        btn_next.setTextColor(R.color.black);
        btn_next.setText("Tampilkan Lainya...");
        list.addFooterView(btn_next);
        list.setFooterDividersEnabled(true);

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventsList.clear();
                adapter.notifyDataSetChanged();
                loadEvent(0);
                txt_alert.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }



    private void loadEvent(int page){

        swipe.setRefreshing(true);

        // Creating volley request obj
        JsonArrayRequest arrReq = new JsonArrayRequest(url_list + page, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    EventData event = new EventData();

                                    no = obj.getInt(TAG_NO);

                                    event.setId_event(obj.getString(TAG_ID));
                                    event.setJudul_event(obj.getString(TAG_JUDUL));

                                    if (obj.getString(TAG_GAMBAR) != "") {
                                        event.setGambar_event(obj.getString(TAG_GAMBAR));
                                    }

                                    event.setTanggal_event(obj.getString(TAG_TGL));
                                    event.setLokasi_event(obj.getString(TAG_LOKASI));
                                    //event.setIsi_event(obj.getString(TAG_ISI));

                                    // adding news to news array

                                    eventsList.add(event);

                                    if (no > offSet)
                                        offSet = no;

                                    Log.d(TAG, "offSet " + offSet);

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }

                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                adapter.notifyDataSetChanged();
                            }
                        }
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
        txt_alert.setVisibility(View.GONE);
    }


    // asynctask class to handle parsing json data searching in background
    public class getDataSearch extends AsyncTask<Void, Void, Void> {

        // show progressbar first
        /*getDataSearch(){
            if(!prgLoading.isShown()){
                prgLoading.setVisibility(0);
                txt_alert.setVisibility(8);
            }
        }*/

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // parse json data from server in background
            //parseJSONData();

            //customListViewLands.imageLoader.clearCache();

            try {
                // request data from menu API
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
                HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
                HttpUriRequest request = new HttpGet(url_search);
                HttpResponse response = client.execute(request);
                InputStream atomInputStream = response.getEntity().getContent();

                BufferedReader in = new BufferedReader(new InputStreamReader(atomInputStream));

                String line;
                String str = "";
                while ((line = in.readLine()) != null){
                    str += line;
                }

                // parse json data and store into arraylist variables
                JSONObject json = new JSONObject(str);
                JSONArray data = json.getJSONArray("data_event"); // this is the "items: [ ] part
                //customListViewLands.imageLoader.clearCache();
                for (int i = 0; i < data.length(); i++) {
                    JSONObject object = data.getJSONObject(i);

                    //SpotData spots = new SpotData();
                    JSONObject event = object.getJSONObject("event");

                    EventData events = new EventData();

                    //no = obj.getInt(TAG_NO);

                    events.setId_event(event.getString(TAG_ID));
                    events.setJudul_event(event.getString(TAG_JUDUL));

                    if (event.getString(TAG_GAMBAR) != "") {
                        events.setGambar_event(event.getString(TAG_GAMBAR));
                    }

                    events.setTanggal_event(event.getString(TAG_TGL));
                    events.setLokasi_event(event.getString(TAG_LOKASI));
                    //spots.setJenis_tempat(spot.getString(TAG_JENIS));

                    // adding news to news array
                    eventsList.add(events);
                }

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            swipe.setRefreshing(false);
            if(eventsList.size() > 0){
                txt_alert.setVisibility(View.GONE);
            }else{
                txt_alert.setVisibility(View.VISIBLE);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        eventsList.clear();
        adapter.notifyDataSetChanged();
        loadEvent(0);
        txt_alert.setVisibility(View.GONE);
    }
}
