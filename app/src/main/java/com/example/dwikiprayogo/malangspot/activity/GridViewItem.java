package com.example.dwikiprayogo.malangspot.activity;

import android.graphics.drawable.Drawable;

public class GridViewItem
{
    String title;
    Drawable image, imageNotif;

    // Empty Constructor
    public GridViewItem()
    {

    }

    // Constructor
    public GridViewItem(String title, Drawable image, Drawable imageNotif)
    {
        super();
        this.title = title;
        this.image = image;
        this.imageNotif = imageNotif;
    }

    // Getter and Setter Method
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Drawable getImage()
    {
        return image;
    }

    public void setImage(Drawable image)
    {
        this.image = image;
    }

    public Drawable getImageNotif()
    {
        return imageNotif;
    }

    public void setImageNotif(Drawable imageNotif)
    {
        this.imageNotif = imageNotif;
    }

}