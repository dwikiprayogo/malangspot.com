package com.example.dwikiprayogo.malangspot.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.adapter.SpotAdapter;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.SpotData;
import com.example.dwikiprayogo.malangspot.fragments.SpotFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class SpotActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    boolean value_anim=true, value_list=false;
    ListView list;
    TextView txt_kategoriId, txt_alert;
    EditText etsearch;
    ImageView img_x;
    SwipeRefreshLayout swipe;
    List<SpotData> spotsList = new ArrayList<SpotData>();

    private static final String TAG = SpotActivity.class.getSimpleName();

    //private static String url_search = Server.url_website + "cari_data.php?keyword=";

    private int offSet = 0;
    private String Keyword, url_search, url_list, jenis_tempat;

    int no, kategori_id;

    SpotAdapter adapter;

    public static final String TAG_NO       = "no";
    public static final String TAG_ID       = "id_tempat";
    public static final String TAG_JENIS    = "jenis_tempat";
    public static final String TAG_NAMA      = "nama_tempat";
    public static final String TAG_ALAMAT   = "alamat";
    public static final String TAG_GAMBAR   = "gambar";

    Handler handler;
    Runnable runnable;

    Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spot);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SpotActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_event);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        list = (ListView) findViewById(R.id.listspot);
        spotsList.clear();
        txt_kategoriId = (TextView) findViewById(R.id.kategori_spot);
        etsearch = (EditText) findViewById(R.id.edtKeyword);
        txt_alert = (TextView) findViewById(R.id.txtAlert);
        img_x = (ImageView) findViewById(R.id.xcross_img);
        img_x.setVisibility(View.GONE);
        list.setVisibility(View.GONE);
        String kategori = sharedPreferences.getString(SpotFragment.kategori_spot, null);
        txt_kategoriId.setText(""+kategori);
        kategori_id = Integer.parseInt(txt_kategoriId.getText().toString());
        //jenis_tempat = txt_kategori.getText().toString();

        url_search = Server.url_search+"?accesskey="+Server.AccessKey+"&jenis_tempat="+kategori_id;
        url_list   = Server.url_website+"?jenis_tempat="+kategori_id+"&offset=";

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(SpotActivity.this, SpotDetail.class);
                //bundle.putString(TAG_ID, spotsList.get(position).getId_tempat());
                //bundle.putString(TAG_JENIS, spotsList.get(position).getJenis_tempat());
                //bundle.putString(TAG_NAMA, spotsList.get(position).getNama_tempat());
                //bundle.putString(TAG_ALAMAT, spotsList.get(position).getAlamat());
                //bundle.putString(TAG_GAMBAR, spotsList.get(position).getGambar());
                //bundle.putString("deskripsi", get_diskirpsi[position]);
                //bundle.putString("latitude",get_latitude[position]);
                //bundle.putString("longitude",get_longitude[position]);
                //intent.putExtras(bundle);
                intent.putExtra(TAG_ID, spotsList.get(position).getId_tempat());
                intent.putExtra(TAG_JENIS, spotsList.get(position).getJenis_tempat());
                startActivity(intent);
                overridePendingTransition(R.anim.open_next, R.anim.close_next);
            }
        });

        img_x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etsearch.getText().clear();
                TranslateAnimation animate = new TranslateAnimation(0, img_x.getWidth(), 0, 0);
                animate.setDuration(500);
                animate.setFillAfter(true);
                img_x.startAnimation(animate);
                img_x.setVisibility(View.GONE);
                txt_alert.setVisibility(View.GONE);
                value_anim = true;
                swipe.setRefreshing(true);
                handler = new Handler();

                runnable = new Runnable() {
                    public void run() {
                        loadSpot(0);
                        txt_alert.setVisibility(View.GONE);
                    }
                };

                handler.postDelayed(runnable, 1000);
            }
        });

        adapter = new SpotAdapter(SpotActivity.this, spotsList);
        list.setAdapter(adapter);

        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           spotsList.clear();
                           adapter.notifyDataSetChanged();
                           loadSpot(0);
                           list.setVisibility(View.VISIBLE);
                           txt_alert.setVisibility(View.GONE);
                           value_list=true;
                       }
                   }
        );


        etsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //loadSpot(offSet);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (value_anim == true) {
                    TranslateAnimation animate = new TranslateAnimation(img_x.getWidth(), 0, 0, 0);
                    animate.setDuration(500);
                    animate.setFillAfter(true);
                    img_x.startAnimation(animate);
                    img_x.setVisibility(View.VISIBLE);
                    value_anim = false;
                    loadSpot(offSet);
                } else {
                    //do nothing
                    try {
                        Keyword = URLEncoder.encode(etsearch.getText().toString(), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    swipe.setRefreshing(true);
                    spotsList.clear();
                    adapter.notifyDataSetChanged();
                    url_search += "&keyword="+Keyword;
                    //IOConnect = 0;
                    list.invalidateViews();
                    new getDataSearch().execute();
                    txt_alert.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        list.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }

            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                        swipe.setRefreshing(true);

                        handler = new Handler();
                        runnable = new Runnable() {
                            public void run() {
                                loadSpot(offSet);
                                txt_alert.setVisibility(View.GONE);
                            }
                        };
                        handler.postDelayed(runnable, 1000);

                }
            }

        });

        Button btn_next = new Button(this);
        btn_next.setBackgroundResource(R.color.colorMaxTransparant);
        btn_next.setTextColor(R.color.black);
        btn_next.setText("Tampilkan Lainya...");
        list.addFooterView(btn_next);
        list.setFooterDividersEnabled(true);

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spotsList.clear();
                adapter.notifyDataSetChanged();
                loadSpot(0);
                txt_alert.setVisibility(View.GONE);
            }
        });

    }

    private void loadSpot(final int page){

        swipe.setRefreshing(true);

        // Creating volley request obj
        final JsonArrayRequest arrReq = new JsonArrayRequest(url_list + page,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    SpotData spot = new SpotData();

                                    no = obj.getInt(TAG_NO);

                                    spot.setId_tempat(obj.getString(TAG_ID));
                                    spot.setNama_tempat(obj.getString(TAG_NAMA));

                                    if (obj.getString(TAG_GAMBAR) != "") {
                                        spot.setGambar(obj.getString(TAG_GAMBAR));
                                    }

                                    spot.setAlamat(obj.getString(TAG_ALAMAT));
                                    spot.setJenis_tempat(obj.getString(TAG_JENIS));

                                    // adding news to news array
                                    spotsList.add(spot);

                                    if (no > offSet) {
                                        offSet = no;
                                        //value_list=false;
                                    }else {

                                    }

                                    Log.d(TAG, "offSet " + offSet);

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }

                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                adapter.notifyDataSetChanged();
                            }
                        }
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);

    }

    // asynctask class to handle parsing json data searching in background
    public class getDataSearch extends AsyncTask<Void, Void, Void> {

        // show progressbar first
        /*getDataSearch(){
            if(!prgLoading.isShown()){
                prgLoading.setVisibility(0);
                txt_alert.setVisibility(8);
            }
        }*/

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // parse json data from server in background
            //parseJSONData();

            //customListViewLands.imageLoader.clearCache();

            try {
                // request data from menu API
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
                HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
                HttpUriRequest request = new HttpGet(url_search);
                HttpResponse response = client.execute(request);
                InputStream atomInputStream = response.getEntity().getContent();

                BufferedReader in = new BufferedReader(new InputStreamReader(atomInputStream));

                String line;
                String str = "";
                while ((line = in.readLine()) != null){
                    str += line;
                }

                // parse json data and store into arraylist variables
                JSONObject json = new JSONObject(str);
                JSONArray data = json.getJSONArray("data_tempat"); // this is the "items: [ ] part
                //customListViewLands.imageLoader.clearCache();
                for (int i = 0; i < data.length(); i++) {
                    JSONObject object = data.getJSONObject(i);

                    //SpotData spots = new SpotData();
                    JSONObject spot = object.getJSONObject("tempat");


                    SpotData spots = new SpotData();

                    //no = obj.getInt(TAG_NO);

                    spots.setId_tempat(spot.getString(TAG_ID));
                    spots.setNama_tempat(spot.getString(TAG_NAMA));

                    if (spot.getString(TAG_GAMBAR) != "") {
                        spots.setGambar(spot.getString(TAG_GAMBAR));
                    }

                    spots.setAlamat(spot.getString(TAG_ALAMAT));
                    spots.setJenis_tempat(spot.getString(TAG_JENIS));

                    // adding news to news array
                    spotsList.add(spots);
                }

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            swipe.setRefreshing(false);
            if(spotsList.size() > 0){
                txt_alert.setVisibility(View.GONE);
            }else{
                txt_alert.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onRefresh() {
        spotsList.clear();
        adapter.notifyDataSetChanged();
        loadSpot(0);
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig)
    {
        // Ignore orientation change to keep activity from restarting
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        //overridePendingTransition(R.anim.open_main, R.anim.close_next);
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
        //clearData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                //clearData();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
