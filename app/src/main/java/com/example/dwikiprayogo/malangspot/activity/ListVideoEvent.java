package com.example.dwikiprayogo.malangspot.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.adapter.AdapterVideo;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.VideoData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListVideoEvent extends AppCompatActivity {

    Toolbar toolbar;
    EditText etsearch;
    TextView txt_kategori,txt_alert;
    ImageView img_x;
    ListView list;
    boolean value_anim=true;
    SwipeRefreshLayout swipe;
    List<VideoData> videosList = new ArrayList<VideoData>();

    private static final String TAG = ListVideo.class.getSimpleName();


    private int offSet = 0;
    private String Keyword, url_search, url_list, jenis_tempat, id_tempat;

    int no, kategori_id;

    AdapterVideo adapter;

    //public static final String TAG_NO       = "no";
    public static final String TAG_ID          = "id_event";
    public static final String TAG_VIDEO       = "url";
    public static final String TAG_VIDEO_IMAGE      = "url_video2";
    //public static final String TAG_TGL      = "tgl_event";
    //public static final String TAG_LOKASI   = "lokasi";
    //public static final String TAG_ISI      = "deskripsi_event";
    //public static final String TAG_GAMBAR   = "gambar";

    Handler handler;
    Runnable runnable;

    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_video_event);

        list = (ListView) findViewById(R.id.listvideo);
        txt_alert = (TextView) findViewById(R.id.txtAlert);

        adapter = new AdapterVideo(ListVideoEvent.this, videosList);
        list.setAdapter(adapter);
        id_tempat = getIntent().getStringExtra(TAG_ID);
        //txt_alert.setText(""+id_tempat);
        url_list = Server.url_event_video+"?id_event=";
        loadList(id_tempat);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(ListVideoEvent.this, VideoActivity.class);
                intent.putExtra(TAG_VIDEO, videosList.get(position).getUrl());
                startActivity(intent);
            }
        });

    }

    private void loadList(String id_tempat){

        //swipe.setRefreshing(true);

        // Creating volley request obj

        JsonArrayRequest arrReq = new JsonArrayRequest(url_list + id_tempat, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, "Response " + response.toString());

                if (response.length() > 0) {
                    // Parsing json
                    for (int i = 0; i < response.length(); i++) {
                        try {

                            JSONObject obj = response.getJSONObject(i);
                            VideoData video = new VideoData();

                            //no = obj.getInt(TAG_NO);
                            video.setUrl(obj.getString(TAG_VIDEO));

                            //artikel.setId_artikel(obj.getString(TAG_ID));
                            video.setUrl_video(obj.getString(TAG_VIDEO_IMAGE));
                            //spot.setJenis_tempat(obj.getString(TAG_JENIS_SPOT));
                            //spot.setNama_tempat(obj.getString(TAG_NAMA));

                            //if (obj.getString(TAG_GAMBAR) != "") {
                            //    artikel.setGambar_artikel(obj.getString(TAG_GAMBAR));
                            // }

                            //artikel.setTanggal_artikel(obj.getString(TAG_TGL));
                            //spot.setDeskripsi(obj.getString(TAG_RELASI));

                            // adding news to news array
                            videosList.add(video);
                            if (videosList.size() > 0){
                                txt_alert.setVisibility(View.INVISIBLE);
                            }else {
                                txt_alert.setVisibility(View.VISIBLE);
                            }

                            //artikelsList.add(artikel);

                            //if (no > offSet)
                            //  offSet = no;

                            Log.d(TAG, "offSet " + offSet);

                        } catch (JSONException e) {
                            Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }
                //swipe.setRefreshing(false);
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);

    }
}
