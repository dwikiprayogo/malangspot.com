package com.example.dwikiprayogo.malangspot.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.adapter.SpotAdapter;
import com.example.dwikiprayogo.malangspot.adapter.TalentAdapter;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.ArtikelData;
import com.example.dwikiprayogo.malangspot.data.SpotData;
import com.example.dwikiprayogo.malangspot.data.TalentData;
import com.example.dwikiprayogo.malangspot.fragments.HomeFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ListTalent extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    Toolbar toolbar;
    EditText etsearch;
    TextView txt_kategori, txt_alert;
    ImageView img_x;
    SwipeRefreshLayout swipe;
    boolean value_anim=true;

    TalentAdapter talentAdapter;
    //List<TalentData> talentsList = new ArrayList<TalentData>();
    ArrayList<TalentData> data_talent = new ArrayList<TalentData>();
    GridView gridview;

    private static final String TAG = ListTalent.class.getSimpleName();

    //private static String url_search = Server.url_website + "cari_data.php?keyword=";

    private int offSet = 0;
    private String Keyword, url_search, url_list, jenis_tempat;

    int no, kategori_id;

    //TalentAdapter adapter;

    public static final String TAG_NO       = "no";
    public static final String TAG_ID       = "id_talent";
    public static final String TAG_JENIS    = "jenis_talent";
    public static final String TAG_NAMA     = "nama_talent";
    public static final String TAG_GAMBAR   = "gambar";

    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_talent);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ListTalent.this);
        txt_kategori = (TextView) findViewById(R.id.kategori_name);
        txt_alert = (TextView) findViewById(R.id.txtAlert);
        gridview = (GridView) findViewById(R.id.gridViewTalent);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        String kategori = sharedPreferences.getString(HomeFragment.kategori_artikel, null);
        txt_kategori.setText(""+kategori);
        //kategori_id = Integer.parseInt(txt_kategori.getText().toString());
        url_list = Server.url_talent+"?jenis_talent="+kategori+"&offset=";
        url_search = Server.url_search_talent+"?accesskey="+Server.AccessKey+"&jenis_talent="+kategori;

        etsearch = (EditText) findViewById(R.id.edtKeyword);
        img_x = (ImageView) findViewById(R.id.xcross_img);
        img_x.setVisibility(View.GONE);

        toolbar = (Toolbar) findViewById(R.id.toolbar_event);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        etsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (value_anim==true){
                    TranslateAnimation animate = new TranslateAnimation(img_x.getWidth(),0,0,0);
                    animate.setDuration(500);
                    animate.setFillAfter(true);
                    img_x.startAnimation(animate);
                    img_x.setVisibility(View.VISIBLE);
                    value_anim=false;
                    loadTalent(offSet);
                }else{
                    //do nothing
                    try {
                        Keyword = URLEncoder.encode(etsearch.getText().toString(), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    swipe.setRefreshing(true);
                    data_talent.clear();
                    //adapter.notifyDataSetChanged();
                    url_search += "&keyword="+Keyword;
                    //IOConnect = 0;
                    gridview.invalidateViews();
                    new getDataSearch().execute();
                    txt_alert.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        img_x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etsearch.getText().clear();
                TranslateAnimation animate = new TranslateAnimation(0,img_x.getWidth(),0,0);
                animate.setDuration(500);
                animate.setFillAfter(true);
                img_x.startAnimation(animate);
                img_x.setVisibility(View.GONE);
                value_anim=true;
                swipe.setRefreshing(true);
                handler = new Handler();

                runnable = new Runnable() {
                    public void run() {
                        loadTalent(0);
                        txt_alert.setVisibility(View.GONE);
                    }
                };

                handler.postDelayed(runnable, 1000);
            }
        });

        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           data_talent.clear();
                           //adapter.notifyDataSetChanged();
                           loadTalent(0);
                           //list.setVisibility(View.VISIBLE);
                           //txt_alert.setVisibility(View.GONE);
                       }
                   }
        );

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(ListTalent.this, DetailTalent.class);
                intent.putExtra(TAG_ID, data_talent.get(position).getId_talent());
                intent.putExtra(TAG_NAMA, data_talent.get(position).getNama_talent());
                //intent.putExtra(TAG_TGL, artikelsList.get(position).getTanggal_artikel());
                startActivity(intent);
                overridePendingTransition(R.anim.open_next, R.anim.close_next);
            }
        });

        //talentsList.clear();
//        talentAdapter.notifyDataSetChanged();
        //loadTalent(0);

    }

    private void loadTalent(int page){

        swipe.setRefreshing(true);

        // Creating volley request obj
        JsonArrayRequest arrReq = new JsonArrayRequest(url_list + page,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    TalentData talent = new TalentData();

                                    no = obj.getInt(TAG_NO);

                                    talent.setId_talent(obj.getString(TAG_ID));
                                    talent.setNama_talent(obj.getString(TAG_NAMA));

                                    if (obj.getString(TAG_GAMBAR) != "") {
                                        talent.setGambar_talent(obj.getString(TAG_GAMBAR));
                                    }

                                    //talent.setJenis_talent(obj.getString(TAG_JENIS));
                                    //spot.setJenis_tempat(obj.getString(TAG_JENIS));

                                    // adding news to news array
                                    data_talent.add(talent);

                                    if (no > offSet)
                                        offSet = no;

                                    Log.d(TAG, "offSet " + offSet);

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }

                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                //talentAdapter.notifyDataSetChanged();
                            }
                        }
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
        txt_alert.setVisibility(View.GONE);
        setDataAdapter();
    }


    private void setDataAdapter() {
        talentAdapter = new TalentAdapter(ListTalent.this, R.layout.grid_talent_adapter, data_talent);
        gridview.setAdapter(talentAdapter);
        /*talentAdapter = new TalentAdapter(ListTalent.this, talentsList);
        gridview.setAdapter(talentAdapter);*/
        //value=false;
    }

    // asynctask class to handle parsing json data searching in background
    public class getDataSearch extends AsyncTask<Void, Void, Void> {

        // show progressbar first
        /*getDataSearch(){
            if(!prgLoading.isShown()){
                prgLoading.setVisibility(0);
                txt_alert.setVisibility(8);
            }
        }*/

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            // parse json data from server in background
            //parseJSONData();

            //customListViewLands.imageLoader.clearCache();

            try {
                // request data from menu API
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 15000);
                HttpConnectionParams.setSoTimeout(client.getParams(), 15000);
                HttpUriRequest request = new HttpGet(url_search);
                HttpResponse response = client.execute(request);
                InputStream atomInputStream = response.getEntity().getContent();

                BufferedReader in = new BufferedReader(new InputStreamReader(atomInputStream));

                String line;
                String str = "";
                while ((line = in.readLine()) != null){
                    str += line;
                }

                // parse json data and store into arraylist variables
                JSONObject json = new JSONObject(str);
                JSONArray data = json.getJSONArray("data_talent"); // this is the "items: [ ] part
                //customListViewLands.imageLoader.clearCache();
                for (int i = 0; i < data.length(); i++) {
                    JSONObject object = data.getJSONObject(i);

                    //SpotData spots = new SpotData();
                    JSONObject talent = object.getJSONObject("talent");

                    TalentData talents = new TalentData();

                    //no = obj.getInt(TAG_NO);

                    talents.setId_talent(talent.getString(TAG_ID));
                    talents.setNama_talent(talent.getString(TAG_NAMA));

                    if (talent.getString(TAG_GAMBAR) != "") {
                        talents.setGambar_talent(talent.getString(TAG_GAMBAR));
                    }

                    //artikels.setIsi_artikel(artikel.getString(TAG_ISI));
                    //artikels.setTanggal_artikel(artikel.getString(TAG_TGL));
                    //spots.setJenis_tempat(spot.getString(TAG_JENIS));

                    // adding news to news array
                    data_talent.add(talents);
                }

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            swipe.setRefreshing(false);
            if(data_talent.size() > 0){
                txt_alert.setVisibility(View.GONE);
                setDataAdapter();
            }else{
                txt_alert.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        data_talent.clear();
        //adapter.notifyDataSetChanged();
        loadTalent(0);
    }
}
