package com.example.dwikiprayogo.malangspot.data;

/**
 * Created by DWIKI PRAYOGO on 08/04/2017.
 */

public class ArtikelData {
    private String id_artikel, judul_artikel, tanggal_artikel, isi_artikel, gambar_artikel;

    public ArtikelData() {
    }

    public ArtikelData(String id_artikel, String judul_artikel, String tanggal_artikel, String isi_artikel, String gambar_artikel) {
        this.id_artikel = id_artikel;
        this.judul_artikel = judul_artikel;
        this.tanggal_artikel = tanggal_artikel;
        this.isi_artikel = isi_artikel;
        this.gambar_artikel = gambar_artikel;
    }

    public String getId_artikel() {
        return id_artikel;
    }

    public void setId_artikel(String id_artikel) {
        this.id_artikel = id_artikel;
    }

    public String getJudul_artikel() {
        return judul_artikel;
    }

    public void setJudul_artikel(String judul_artikel) {
        this.judul_artikel = judul_artikel;
    }

    public String getTanggal_artikel() {
        return tanggal_artikel;
    }

    public void setTanggal_artikel(String tanggal_artikel) {
        this.tanggal_artikel = tanggal_artikel;
    }

    public String getIsi_artikel() {
        return isi_artikel;
    }

    public void setIsi_artikel(String isi_artikel) {
        this.isi_artikel = isi_artikel;
    }

    public String getGambar_artikel() {
        return gambar_artikel;
    }

    public void setGambar_artikel(String gambar_artikel) {
        this.gambar_artikel = gambar_artikel;
    }
}
