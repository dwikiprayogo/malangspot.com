package com.example.dwikiprayogo.malangspot.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by DWIKI PRAYOGO on 15/03/2017.
 */

@SuppressLint("CommitPrefEdits")
public class SharedPreference {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // nama sharepreference
    private static final String PREF_NAME = "Sesi";
    // All Shared Preferences Keys
    private static final String IS_EVENT = "IsEventIn";
    private static final String IS_ARTIKEL = "IsArtikelIn";
    private static final String IS_IKLAN = "IsIklanIn";
    private static final String IS_TALENT = "IsTalentIn";
    public static final String KEY_EVENT = "event";
    public static final String KEY_ARTIKEL = "artikel";
    public static final String KEY_IKLAN = "iklan";
    public static final String KEY_TALENT = "talent";


    // Constructor
    public SharedPreference(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void EventNotif(String event){
        // Storing login value as TRUE
        editor.putBoolean(IS_EVENT, false);
        editor.putString(KEY_EVENT, event);
        editor.commit();
    }

    public void ArtikelNotif(String artikel){
        // Storing login value as TRUE
        editor.putBoolean(IS_ARTIKEL, false);
        editor.putString(KEY_ARTIKEL, artikel);
        editor.commit();
    }

    public void IklanNotif(String iklan){
        // Storing login value as TRUE
        editor.putBoolean(IS_IKLAN, false);
        editor.putString(KEY_IKLAN, iklan);
        editor.commit();
    }

    public void TalentNotif(String talent){
        // Storing login value as TRUE
        editor.putBoolean(IS_TALENT, false);
        editor.putString(KEY_TALENT, talent);
        editor.commit();
    }


    /*public void get_data(String jumlah_hit, String jumlah_tim, String jumlah_kal){
        // Storing login value as TRUE
        editor.putString(Hitungan, jumlah_hit);
        editor.putString(Timer, jumlah_tim);
        editor.putString(Kalori, jumlah_kal);
        editor.commit();
    }*/

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    /*public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
            //((Activity)_context).finish();
        }

    }*/

    /**
     * Get stored session data
     * */
    /*public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_HEIGHT, pref.getString(KEY_HEIGHT, null));
        user.put(KEY_WEIGHT, pref.getString(KEY_WEIGHT, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_PASS, pref.getString(KEY_PASS, null));

        return user;
    }*/

    public HashMap<String, String> getDataNotifEvent(){
        HashMap<String, String> event_notif = new HashMap<String, String>();
        event_notif.put(KEY_EVENT, pref.getString(KEY_EVENT, null));
        return event_notif;
    }

    public HashMap<String, String> getDataNotifArtikel(){
        HashMap<String, String> artikel_notif = new HashMap<String, String>();
        artikel_notif.put(KEY_IKLAN, pref.getString(KEY_ARTIKEL, null));
        return artikel_notif;
    }

    public HashMap<String, String> getDataNotifIklan(){
        HashMap<String, String> iklan_notif = new HashMap<String, String>();
        iklan_notif.put(KEY_IKLAN, pref.getString(KEY_IKLAN, null));
        return iklan_notif;
    }

    public HashMap<String, String> getDataNotifTalent(){
        HashMap<String, String> talent_notif = new HashMap<String, String>();
        talent_notif.put(KEY_TALENT, pref.getString(KEY_TALENT, null));
        return talent_notif;
    }


    /**
     * Clear session details
     * */
    /*public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }*/

    public boolean isEventIn(){
        return pref.getBoolean(IS_EVENT, true);
    }

    public boolean isArtikelIn(){
        return pref.getBoolean(IS_ARTIKEL, true);
    }

    public boolean isIklanIn(){
        return pref.getBoolean(IS_IKLAN, true);
    }
    public boolean isTalentIn(){
        return pref.getBoolean(IS_TALENT, true);
    }
}
