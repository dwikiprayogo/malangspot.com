package com.example.dwikiprayogo.malangspot.data;

/**
 * Created by DWIKI PRAYOGO on 14/04/2017.
 */

public class EventData {
    private String id_event, judul_event, tanggal_event, isi_event, lokasi_event ,gambar_event;

    public EventData() {
    }

    public EventData(String id_event, String judul_event, String tanggal_event, String isi_event, String lokasi_event ,String gambar_event) {
        this.id_event = id_event;
        this.judul_event = judul_event;
        this.tanggal_event = tanggal_event;
        this.isi_event = isi_event;
        this.lokasi_event = lokasi_event;
        this.gambar_event = gambar_event;
    }

    public String getId_event() {
        return id_event;
    }

    public void setId_event(String id_event) {
        this.id_event = id_event;
    }

    public String getJudul_event() {
        return judul_event;
    }

    public void setJudul_event(String judul_event) {
        this.judul_event = judul_event;
    }

    public String getTanggal_event() {
        return tanggal_event;
    }

    public void setTanggal_event(String tanggal_event) {
        this.tanggal_event = tanggal_event;
    }

    public String getIsi_event() {
        return isi_event;
    }

    public void setIsi_event(String isi_event) {
        this.isi_event = isi_event;
    }

    public String getLokasi_event(){
        return lokasi_event;
    }

    public  void setLokasi_event(String lokasi_event){
        this.lokasi_event=lokasi_event;
    }

    public String getGambar_event() {
        return gambar_event;
    }

    public void setGambar_event(String gambar_event) {
        this.gambar_event = gambar_event;
    }

}
