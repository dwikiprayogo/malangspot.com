package com.example.dwikiprayogo.malangspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.ArtikelData;
import com.example.dwikiprayogo.malangspot.data.SpotData;
import com.example.dwikiprayogo.malangspot.fragments.AboutFragment;

import java.util.List;

/**
 * Created by DWIKI PRAYOGO on 08/04/2017.
 */

public class ArtikelAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<ArtikelData> artikelsItems;
    ImageLoader imageLoader;

    public ArtikelAdapter(Activity act, List<ArtikelData> artikelsItems) {
        this.activity = act;
        this.artikelsItems = artikelsItems;
        //imageLoader = new ImageLoader(act);
    }

    @Override
    public int getCount() {
        return artikelsItems.size();
    }

    @Override
    public Object getItem(int location) {
        return artikelsItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.custom_listviewpotrait, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        TextView txtTextJudul = (TextView) convertView.findViewById(R.id.txt_judulArtikel);
        TextView txtTgl = (TextView) convertView.findViewById(R.id.txt_tgl);
        TextView txtisi = (TextView)  convertView.findViewById(R.id.txt_isiArtikel);
        NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.artikel_gambar);

        ArtikelData artikels = artikelsItems.get(position);

        thumbNail.setImageUrl(Server.url_imagelist_artikel+artikels.getGambar_artikel(), imageLoader);
        txtTextJudul.setText(artikels.getJudul_artikel());
        txtTgl.setText(artikels.getTanggal_artikel());
        txtisi.setText(Html.fromHtml(artikels.getIsi_artikel()));

        return convertView;
    }
}
