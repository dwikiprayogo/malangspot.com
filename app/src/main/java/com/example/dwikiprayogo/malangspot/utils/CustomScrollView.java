package com.example.dwikiprayogo.malangspot.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Created by DWIKI PRAYOGO on 20/04/2017.
 */

public class CustomScrollView extends ScrollView {

    public CustomScrollView(Context context){
        super(context);
    }

    public CustomScrollView (Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public CustomScrollView (Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        switch (action){
            case MotionEvent.ACTION_DOWN:
                Log.i("VerticalScrollView","OnInterceptTouchEvent : Down Super false");
                super.onTouchEvent(ev);
                break;

            case MotionEvent.ACTION_MOVE:
                return false;

            case MotionEvent.ACTION_CANCEL:
                Log.i("VerticalScrollView","onInterceptTouchEvent : Cancel super false");
                super.onTouchEvent(ev);
                break;

            default : Log.i("VerticalScrollView","onInterceptTouchEvent : " + action);
                break;
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        super.onTouchEvent(ev);
        Log.i("VerticalScrollView","onTouch. action: " + ev.getAction());
        return true;
    }
}
