package com.example.dwikiprayogo.malangspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.ArtikelData;
import com.example.dwikiprayogo.malangspot.data.EventData;

import java.util.List;

/**
 * Created by DWIKI PRAYOGO on 14/04/2017.
 */

public class EventAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<EventData> eventsItems;
    ImageLoader imageLoader;

    public EventAdapter(Activity act, List<EventData> eventsItems) {
        this.activity = act;
        this.eventsItems = eventsItems;
        //imageLoader = new ImageLoader(act);
    }

    @Override
    public int getCount() {
        return eventsItems.size();
    }

    @Override
    public Object getItem(int location) {
        return eventsItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.custom_listviewevent, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        TextView txtTextJudul = (TextView) convertView.findViewById(R.id.txt_judulArtikel);
        TextView txtTgl = (TextView) convertView.findViewById(R.id.txt_tgl);
        //TextView txtisi = (TextView)  convertView.findViewById(R.id.txt_isiArtikel);
        TextView txtlokasi = (TextView) convertView.findViewById(R.id.txt_lokasi);
        NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.artikel_gambar);

        EventData events = eventsItems.get(position);

        thumbNail.setImageUrl(Server.url_imagelist_event+events.getGambar_event(), imageLoader);
        txtTextJudul.setText(events.getJudul_event());
        txtTgl.setText(events.getTanggal_event());
        txtlokasi.setText(events.getLokasi_event());
        //txtisi.setText(Html.fromHtml(events.getIsi_event()));

        return convertView;
    }

}

