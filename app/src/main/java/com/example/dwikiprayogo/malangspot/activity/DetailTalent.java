package com.example.dwikiprayogo.malangspot.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.adapter.AdapterKomentar;
import com.example.dwikiprayogo.malangspot.app.AppController;

import com.example.dwikiprayogo.malangspot.data.KomentarData;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayerView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetailTalent extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    Toolbar toolbar;

    Integer id;
    Integer lenght_des;
    String judul,deskripsi,gambar,latitude,longitude,alamat;
    String[] data_gambar;
    long Spot_id;
    SwipeRefreshLayout swipe;
    ListView listkomen;
    private int offSet = 0;
    int no;
    int successs;
    String Spot_kategori,Spot_nama,Spot_alamat,Spot_deskripsi,Spot_latitude,Spot_longitude,Spot_gambar;
    VideoView talentvideo;
    String url_detailTalent,url_imageTalent,url_list,jenis_komentar="video_talent";
    //ImageLoader imageLoader;
    TextView txt_namaTalent,txt_kategoriTalent,txtAlert, txt_deskripsiTalent, txt_idArtikel, txtId_Artikel, url_video;
    ScrollView sv_detailArtikel;
    WebView wb_detail;
    Button btn_komen,btn_video,btn_lokasi,btn_rekom;
    ProgressBar prgLoading;
    int IOConnect = 0;
    ImageView img_detail;
    //CustomListViewLands customListViewLands;
    //Bundle item;

    ProgressDialog pDialog;
    JSONParser json;
    JSONParser jsonParser = new JSONParser();
    String success;
    JSONArray jsonArray;
    JSONObject jsonObject;
    List<NameValuePair> list;

    List<KomentarData> komensList = new ArrayList<KomentarData>();
    AdapterKomentar adapter;

    Handler handler;
    Runnable runnable;

    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private boolean isFullscreen;

    private int millis=0;

    String[] post_url_image;
    Integer length_json=0;

    ViewPager vpImageTalent;

    ImageAdapterTalent sliderAdapterTalent;

    private static final String TAG = ArtikelDetail.class.getSimpleName();

    public static final String TAG_NO       = "no";
    public static final String TAG_ID       = "id_talent";
    public static final String TAG_JENIS    = "jenis_talent";
    public static final String TAG_NAMA     = "nama_talent";
    public static final String TAG_DESKRIPSI= "deskripsi";
    public static final String TAG_VIDEO   = "url";
    public static final String TAG_GAMBAR   = "gambar";

    //public static final String TAG_NO    = "no";
    public static final String TAG_NAME    = "nama";
    public static final String TAG_EMAIL   = "email";
    public static final String TAG_KOMEN   = "komen";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    //public static final String TAG_GAMBAR	= "gambar";

    //private static final String url_detail 	= Server.URL + "detail_news.php";
    String tag_json_obj = "json_obj_req";

    //private static String url = Server.link_image_detail_restaurant;

    Boolean cek_back=false;
    String url_image, id_talent, tgl_artikel, nama_talent;

    //Dialog dialog;
    FragmentManager fm;

    AlertDialog.Builder dialog_komen, dialog_video;
    LayoutInflater inflater;
    View dialogView;
    ImageView img;
    EditText tv_nama,tv_email,tv_komentar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_talent);

        toolbar = (Toolbar) findViewById(R.id.toolbar_detailArtikel);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        nama_talent = getIntent().getStringExtra(TAG_NAMA);
        id_talent = getIntent().getStringExtra(TAG_ID);
        //judul_artikel = getIntent().getStringExtra(TAG_JUDUL);
        //tgl_artikel = getIntent().getStringExtra(TAG_TGL);
        txt_namaTalent = (TextView) findViewById(R.id.txt_namaTalent);
        url_video = (TextView) findViewById(R.id.url_video);
        txt_kategoriTalent = (TextView) findViewById(R.id.txt_kategoriTalent);
        txt_deskripsiTalent = (TextView) findViewById(R.id.txt_deskripsiTalent);
        btn_komen = (Button)findViewById(R.id.btn_foto);
        btn_video = (Button) findViewById(R.id.btn_video);
        //txt_tglArtikel = (TextView) findViewById(R.id.txt_tglArtikel);
        //txt_deskripsiArtikel = (TextView) findViewById(R.id.txt_deskripsiArtikel);
        txt_namaTalent.setText(""+nama_talent);
        //txt_tglArtikel.setText(""+tgl_artikel);
        vpImageTalent = (ViewPager) findViewById(R.id.vpImageTalent);

        url_detailTalent = Server.url_detaildataTalent;
        url_imageTalent = Server.url_imageTalent+"id_talent="+id_talent;
        url_list   = Server.url_komen+"?jenis_komentar="+jenis_komentar+"&id_relasi="+id_talent+"&offset=";
        //url_imageSpot = Server.url_image_spot+"id_tempat="+txt_id.getText().toString();
        //url_imageArtikel = Server.url_image_artikel+"id_artikel="+id_artikel;
        //url_detailArtikel = Server.url_detaildataArtikel;
        callDetailTalent(id_talent);
        new load_imageTalent().execute();

        btn_komen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm("", "", "", "KIRIM");
            }
        });

        btn_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailTalent.this, VideoActivity.class);
                intent.putExtra(TAG_VIDEO, url_video.getText().toString());
                startActivity(intent);
            }
        });


    }

    // untuk menampilkan dialog from komen
    private void DialogForm(String nama_user, String email_user, String komentar, String button) {
        dialog_komen = new AlertDialog.Builder(DetailTalent.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.dialog_komentar, null);
        dialog_komen.setView(dialogView);
        dialog_komen.setCancelable(true);
        //dialog.setIcon(R.mipmap.ic_launcher);
        //dialog.setTitle("Form Biodata");

        img = (ImageView) dialogView.findViewById(R.id.imgLogo);
        tv_nama = (EditText) dialogView.findViewById(R.id.tv_nama);
        tv_email = (EditText) dialogView.findViewById(R.id.tv_email);
        tv_komentar = (EditText) dialogView.findViewById(R.id.tv_komentar);
        swipe = (SwipeRefreshLayout) dialogView.findViewById(R.id.swipeToRefresh);
        listkomen = (ListView) dialogView.findViewById(R.id.listkomen);

        adapter = new AdapterKomentar(DetailTalent.this, komensList);
        listkomen.setAdapter(adapter);


        swipe.setRefreshing(true);
        swipe.setOnRefreshListener(this);

        komensList.clear();
        loadKomen(0);
        adapter.notifyDataSetChanged();

        //txt_id      = (EditText) dialogView.findViewById(R.id.txt_id);
        //txt_nama    = (EditText) dialogView.findViewById(R.id.txt_nama);
        //txt_alamat  = (EditText) dialogView.findViewById(R.id.txt_alamat);

        /*if (!idx.isEmpty()){
            txt_id.setText(idx);
            txt_nama.setText(namax);
            txt_alamat.setText(alamatx);
        } else {
            kosong();
        }*/

        dialog_komen.setPositiveButton(button, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                /*id      = txt_id.getText().toString();
                nama    = txt_nama.getText().toString();
                alamat  = txt_alamat.getText().toString();

                simpan_update();*/
                simpan();
            }
        });

        dialog_komen.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //kosong();
            }
        });

        listkomen.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }

            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                    swipe.setRefreshing(true);

                    handler = new Handler();
                    runnable = new Runnable() {
                        public void run() {
                            loadKomen(offSet);
                            //txt_alert.setVisibility(View.GONE);
                        }
                    };
                    handler.postDelayed(runnable, 1000);

                }
            }

        });

        dialog_komen.show();
    }

    private void callDetailTalent(final String id_talent){
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading On Progress...");
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        //swipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.POST, url_detailTalent, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response " + response.toString());
                //swipe.setRefreshing(false);

                try {
                    JSONObject obj = new JSONObject(response);

                    String id_tempat    = obj.getString(TAG_ID);
                    String nama_talent   = obj.getString(TAG_NAMA);
                    String jenis_talent = obj.getString(TAG_JENIS);
                    String deskripsi      = obj.getString(TAG_DESKRIPSI);
                    String url = obj.getString(TAG_VIDEO);
                    //String latitude     = obj.getString(TAG_LATITUDE);
                    //String longitude     = obj.getString(TAG_LONGITUDE);
                    //String alamat = obj.getString(TAG_ALAMAT);
                    //String Isi      = obj.getString(TAG_ISI);

                    //txt_judul.setText(nama_tempat);
                    //txt_idKategori.setText(id_tempat);
                    txt_kategoriTalent.setText(jenis_talent);
                    txt_deskripsiTalent.setText(Html.fromHtml(deskripsi));
                    url_video.setText(url);
                    //wb_detail.loadData("<p style=\"text-align: justify\">" + item.getString("deskripsi") + "</p>", "text/html" , "UTF-8");
                    //txt_latitude.setText(latitude);
                    //txt_longitude.setText(longitude);
                    //txt_alamat.setText(alamat);

                    /*if (obj.getString(TAG_GAMBAR)!=""){
                        //thumb_image.setImageUrl(Gambar, imageLoader);
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Detail Spot Error: " + error.getMessage());
                Toast.makeText(DetailTalent.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //swipe.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_talent", id_talent);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        //linearLayout.setVisibility(View.VISIBLE);
        progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        //overridePendingTransition(R.anim.open_main, R.anim.close_next);
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
        //customListViewLands.imageLoader.clearCache();
        //clearData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                //customListViewLands.imageLoader.clearCache();
                //clearData();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {

    }

    public class load_imageTalent extends AsyncTask<String, String, String>
    {


        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(DetailTalent.this);
            pDialog.setIndeterminate(false);

        }

        @Override
        protected String doInBackground(String... arg0) {


            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(url_imageTalent);

            try {
                success = json.getString("success");

                Log.e("error", "nilai sukses=" + success);

                JSONArray hasil = json.getJSONArray("data_gambar");

                if (success.equals("1")) {

                    data_gambar = new String[hasil.length()];
                    for (int i = 0; i < hasil.length(); i++) {

                        JSONObject c = hasil.getJSONObject(i);
                        data_gambar[i] = c.getString("gambar".trim());
                        //session.get_data(jumlah_hit,jumlah_tim,jumlah_kal);
                        Log.e("ok", " ambil data");
                    }
                } else {
                    Log.e("error", "tidak bisa ambil data 0");
                }

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("error", "tidak bisa ambil data 1");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (success.equals("1")) {
                //linearLayout.setVisibility(View.VISIBLE);
                //Toast.makeText(getApplicationContext(), "Data sukses masuk broo ...", Toast.LENGTH_SHORT).show();
                sliderAdapterTalent = new ImageAdapterTalent(DetailTalent.this,data_gambar);
                vpImageTalent.setAdapter(sliderAdapterTalent);
            } else {
                Toast.makeText(getApplicationContext(), "Sorry, failed to response ...", Toast.LENGTH_SHORT).show();
            }
        }

    }

    // fungsi untuk menyimpan atau update
    private void simpan() {
        String url;
        // jika id kosong maka simpan, jika id ada nilainya maka update
        url = Server.url_insert_komen;

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    successs = jObj.getInt(TAG_SUCCESS);

                    // Cek error node pada json
                    if (successs == 1) {
                        Log.d("Add/update", jObj.toString());

                        komensList.clear();
                        adapter.notifyDataSetChanged();
                        loadKomen(0);
                        //callVolley();
                        //kosong();

                        Toast.makeText(DetailTalent.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        adapter.notifyDataSetChanged();

                    } else {
                        Toast.makeText(DetailTalent.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(DetailTalent.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                // jika id kosong maka simpan, jika id ada nilainya maka update
                params.put("id_relasi", id_talent);
                params.put("nama", tv_nama.getText().toString());
                params.put("email", tv_email.getText().toString());
                params.put("komen", tv_komentar.getText().toString());
                params.put("jenis_komentar", jenis_komentar);

                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void loadKomen(final int page){

        swipe.setRefreshing(true);

        // Creating volley request obj
        final JsonArrayRequest arrReq = new JsonArrayRequest(url_list + page,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    KomentarData komen = new KomentarData();

                                    no = obj.getInt(TAG_NO);

                                    komen.setNama(obj.getString(TAG_NAME));
                                    komen.setEmail(obj.getString(TAG_EMAIL));

                                    komen.setKomen(obj.getString(TAG_KOMEN));
                                    //spot.setJenis_tempat(obj.getString(TAG_JENIS));

                                    // adding news to news array
                                    komensList.add(komen);

                                    if (no > offSet) {
                                        offSet = no;
                                    }

                                    Log.d(TAG, "offSet " + offSet);

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }

                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                adapter.notifyDataSetChanged();
                            }
                        }
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);

    }

}
