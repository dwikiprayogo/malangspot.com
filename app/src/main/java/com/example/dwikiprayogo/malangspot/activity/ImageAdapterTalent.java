package com.example.dwikiprayogo.malangspot.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.dwikiprayogo.malangspot.Server;
import com.squareup.picasso.Picasso;

/**
 * Created by DWIKI PRAYOGO on 11/04/2017.
 */

public class ImageAdapterTalent extends PagerAdapter {
    Context mContext;
    String[] url_talent;
    Bitmap bmp;

    ImageAdapterTalent(Context context, String[] url_talent) {
        this.mContext = context;
        this.url_talent = url_talent;
    }
    @Override
    public int getCount() {
        return url_talent.length;
    }

    @Override
    public boolean isViewFromObject(View v, Object obj) {
        return v == ((ImageView) obj);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        //imageView.setScaleY(SubsamplingScaleImageView.SCALE_TYPE_CENTER_CROP);
        Picasso.with(container.getContext())
                .load(Server.url_imagedetail_talent+url_talent[i])
                .into(imageView); //letakan gambar yang telah diload kedalam img*/
        //mImageView.setImageBitmap(bmp);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }
    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        ((ViewPager) container).removeView((ImageView) obj);
    }
}
