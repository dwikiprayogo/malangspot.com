package com.example.dwikiprayogo.malangspot.data;

/**
 * Created by DWIKI PRAYOGO on 26/04/2017.
 */

public class KomentarData {
    private String nama, email, komen;

    public KomentarData() {
    }

    public KomentarData(String nama, String email, String komen) {
        this.nama = nama;
        this.email = email;
        this.komen = komen;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKomen() {
        return komen;
    }

    public void setKomen(String komen) {
        this.komen = komen;
    }
}
