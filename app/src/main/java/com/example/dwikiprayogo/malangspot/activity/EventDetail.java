package com.example.dwikiprayogo.malangspot.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.adapter.AdapterKomentar;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.KomentarData;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventDetail extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    Toolbar toolbar;

    Integer id;
    Integer lenght_des;
    String judul,deskripsi,gambar,latitude,longitude,alamat;
    String[] data_gambar;
    SwipeRefreshLayout swipe;
    ListView listkomen;
    private int offSet = 0;
    int no;
    long Spot_id;
    int successs;
    String Spot_kategori,Spot_nama,Spot_alamat,Spot_deskripsi,Spot_latitude,Spot_longitude,Spot_gambar;
    String url_detailEvent,url_imageEvent,url_list,jenis_komentar="video_event",url_webview="http://malangspot.com/spot/jenis/Hotel";
    //ImageLoader imageLoader;
    TextView txt_namaEvent,txt_tglEvent,txt_lokasi,txtAlert,
            txt_deskripsiEvent, txt_idEvent, txt_lat, txt_long;
    ScrollView sv_detailArtikel;
    WebView wb_detail;
    Button btn_foto,btn_video,btn_lokasi,btn_rekom;
    ProgressBar prgLoading;
    int IOConnect = 0;
    ImageView img_detail;
    //CustomListViewLands customListViewLands;
    //Bundle item;

    ProgressDialog pDialog;
    JSONParser json;
    JSONParser jsonParser = new JSONParser();
    String success;
    JSONArray jsonArray;
    JSONObject jsonObject;
    List<NameValuePair> list;

    List<KomentarData> komensList = new ArrayList<KomentarData>();
    AdapterKomentar adapter;

    Handler handler;
    Runnable runnable;

    String[] post_url_image;
    Integer length_json=0;

    ViewPager vpImageEvent;

    ImageAdapterEvent sliderAdapterEvent;

    private static final String TAG = EventDetail.class.getSimpleName();

    public static final String TAG_ID 		    = "id_event";
    public static final String TAG_JUDUL	    = "nama_event";
    public static final String TAG_TGL 		    = "tgl_event";
    public static final String TAG_CONTACT 		= "contact";
    public static final String TAG_LOKASI 	    = "lokasi";
    public static final String TAG_LAT 	        = "latitude";
    public static final String TAG_LONG 	    = "longitude";
    public static final String TAG_DESKRIPSI 	= "deskripsi";
    public static final String TAG_VIDEO = "url";
    //public static final String TAG_DESKRIPSI= "deskripsi";

    public static final String TAG_NO    = "no";
    public static final String TAG_NAME    = "nama";
    public static final String TAG_EMAIL   = "email";
    public static final String TAG_KOMEN   = "komen";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    //public static final String TAG_GAMBAR	= "gambar";

    //private static final String url_detail 	= Server.URL + "detail_news.php";
    String tag_json_obj = "json_obj_req";

    //private static String url = Server.link_image_detail_restaurant;

    Boolean cek_back=false;
    String url_image, id_event, tgl_event, judul_event;

    Dialog dialog;
    FragmentManager fm;

    private TrackGPS gps;
    double longitudes;
    double latitudes;

    public static final String MyREF="mypres";
    public static final String Long="longKey";
    public static final String Lat="latKey";
    public static final String LongS="longSKey";
    public static final String LatS="latSKey";
    public static final String Tempat="TempatKey";
    public static final String Alamat="AlamatKey";
    public static final String Kategori="KategoriKey";

    SharedPreferences sharedPreferences;

    AlertDialog.Builder dialog_komen;
    LayoutInflater inflater;
    View dialogView;
    ImageView img;
    EditText tv_nama,tv_email,tv_komentar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar_detailArtikel);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        fm =getSupportFragmentManager();

        id_event = getIntent().getStringExtra(TAG_ID);
        judul_event = getIntent().getStringExtra(TAG_JUDUL);
        tgl_event = getIntent().getStringExtra(TAG_TGL);
        txt_namaEvent = (TextView) findViewById(R.id.txt_namaEvent);
        txt_lat = (TextView) findViewById(R.id.latitude);
        txt_long =(TextView) findViewById(R.id.longitude);
        txt_deskripsiEvent = (TextView) findViewById(R.id.txt_deskripsiEvent);
        btn_lokasi = (Button) findViewById(R.id.btn_lokasi);
        btn_foto=(Button) findViewById(R.id.btn_foto);
        btn_rekom=(Button) findViewById(R.id.btn_rekom);
        btn_video=(Button) findViewById(R.id.btn_video);
        txt_tglEvent = (TextView) findViewById(R.id.txt_tglEvent);
        txt_lokasi = (TextView) findViewById(R.id.txt_lokasi);
        txt_idEvent=(TextView) findViewById(R.id.txt_idEvent);
        wb_detail = (WebView) findViewById(R.id.detail_view);
        //txt_deskripsiArtikel = (TextView) findViewById(R.id.txt_deskripsiArtikel);
        txt_namaEvent.setText(""+judul_event);
        txt_tglEvent.setText(""+tgl_event);
        txt_idEvent.setText(""+id_event);
        vpImageEvent = (ViewPager) findViewById(R.id.vpImageEvent);


        btn_rekom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sharedPreferences=getSharedPreferences(MyREF, Context.MODE_PRIVATE);
                //SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(EventDetail.this);
                //SharedPreferences.Editor editor=sharedPreferences.edit();
                //editor.putString(Kategori, String.valueOf(txt_idKategori.getText().toString()));
               //editor.commit();
                startActivity(new Intent(EventDetail.this, RecomenEvent.class) );
                overridePendingTransition(R.anim.swip_next, R.anim.swipe_close);
            }
        });

        btn_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm("", "", "", "KIRIM");
            }
        });

        btn_lokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gps = new TrackGPS(EventDetail.this);

                if(gps.canGetLocation()){

                    longitudes = gps.getLongitude();
                    latitudes = gps .getLatitude();
                    sharedPreferences=getSharedPreferences(MyREF, Context.MODE_PRIVATE);
                    SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(EventDetail.this);
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString(Long, String.valueOf(longitudes));
                    editor.putString(Lat, String.valueOf(latitudes));
                    editor.putString(LongS, String.valueOf(txt_long.getText().toString()));
                    editor.putString(LatS, String.valueOf(txt_lat.getText().toString()));
                    editor.putString(Tempat, String.valueOf(txt_lokasi.getText().toString()));
                    //editor.putString(Alamat, String.valueOf(txt_lokasi.getText().toString()));
                    editor.commit();
                    startActivity(new Intent(EventDetail.this,MapEvent.class));
                    overridePendingTransition(R.anim.open_next, R.anim.close_next);

                    //Toast.makeText(getApplicationContext(),"Longitude:"+ Double.toString(longitudes)+"\nLatitude:"+ Double.toString(latitudes), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    gps.showSettingsAlert();
                }
            }
        });

        btn_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EventDetail.this, ListVideoEvent.class);
                intent.putExtra(TAG_ID, txt_idEvent.getText().toString());
                startActivity(intent);
            }
        });

        url_imageEvent = Server.url_imageEvent+"id_event="+id_event;
        url_list   = Server.url_komen+"?jenis_komentar="+jenis_komentar+"&id_relasi="+id_event+"&offset=";
        url_detailEvent = Server.url_detaildataEvent;
        callDetailEvent(id_event);
        new load_imageEvent().execute();
        wb_detail.loadUrl(url_webview);

    }

    private void callDetailEvent(final String id_event){
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading On Progress");
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        //swipe.setRefreshing(true);

        final StringRequest strReq = new StringRequest(Request.Method.POST, url_detailEvent, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response " + response.toString());
                //swipe.setRefreshing(false);

                try {
                    JSONObject obj = new JSONObject(response);

                    String nama_event = obj.getString(TAG_JUDUL);
                    String lokasi    = obj.getString(TAG_LOKASI);
                    String tanggal_event = obj.getString(TAG_TGL);
                    String contact = obj.getString(TAG_CONTACT);
                    String latitude = obj.getString(TAG_LAT);
                    String longitude = obj.getString(TAG_LONG);
                    String deskripsi_event = obj.getString(TAG_DESKRIPSI);
                    //String Isi      = obj.getString(TAG_ISI);

                    //txt_judul.setText(nama_tempat);
                    txt_lokasi.setText(lokasi);
                    txt_lat.setText(latitude);
                    txt_long.setText(longitude);
                    //txt_idKategori.setText(id_tempat);
                    //txt_kategori.setText(jenis_tempat);
                    txt_deskripsiEvent.setText(Html.fromHtml(deskripsi_event));
                    //wb_detail.loadData("<p style=\"text-align: justify\">" + item.getString("deskripsi") + "</p>", "text/html" , "UTF-8");
                    //txt_latitude.setText(latitude);
                    //txt_longitude.setText(longitude);
                    //txt_alamat.setText(alamat);

                    /*if (obj.getString(TAG_GAMBAR)!=""){
                        //thumb_image.setImageUrl(Gambar, imageLoader);
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Detail Event Error: " + error.getMessage());
                Toast.makeText(EventDetail.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //swipe.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_event", id_event);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        //linearLayout.setVisibility(View.VISIBLE);
        progressDialog.dismiss();
    }

    @Override
    public void onRefresh() {
        komensList.clear();
        adapter.notifyDataSetChanged();
        loadKomen(0);
    }

    public class load_imageEvent extends AsyncTask<String, String, String>
    {


        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EventDetail.this);
            pDialog.setIndeterminate(false);

        }

        @Override
        protected String doInBackground(String... arg0) {


            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(url_imageEvent);

            try {
                success = json.getString("success");

                Log.e("error", "nilai sukses=" + success);

                JSONArray hasil = json.getJSONArray("data_gambar");

                if (success.equals("1")) {

                    data_gambar = new String[hasil.length()];
                    for (int i = 0; i < hasil.length(); i++) {

                        JSONObject c = hasil.getJSONObject(i);
                        data_gambar[i] = c.getString("gambar".trim());
                        //session.get_data(jumlah_hit,jumlah_tim,jumlah_kal);
                        Log.e("ok", " ambil data");
                    }
                } else {
                    Log.e("error", "tidak bisa ambil data 0");
                }

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("error", "tidak bisa ambil data 1");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (success.equals("1")) {
                //linearLayout.setVisibility(View.VISIBLE);
                //Toast.makeText(getApplicationContext(), "Data sukses masuk broo ...", Toast.LENGTH_SHORT).show();
                sliderAdapterEvent = new ImageAdapterEvent(EventDetail.this,data_gambar);
                vpImageEvent.setAdapter(sliderAdapterEvent);
            } else {
                Toast.makeText(getApplicationContext(), "Sorry, failed to response ...", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onBackPressed() {
        //overridePendingTransition(R.anim.open_main, R.anim.close_next);
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
        //customListViewLands.imageLoader.clearCache();
        //clearData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                //customListViewLands.imageLoader.clearCache();
                //clearData();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // untuk menampilkan dialog from komen
    private void DialogForm(String nama_user, String email_user, String komentar, String button) {
        dialog_komen = new AlertDialog.Builder(EventDetail.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.dialog_komentar, null);
        dialog_komen.setView(dialogView);
        dialog_komen.setCancelable(false);
        //dialog.setIcon(R.mipmap.ic_launcher);
        //dialog.setTitle("Form Biodata");

        img = (ImageView) dialogView.findViewById(R.id.imgLogo);
        tv_nama = (EditText) dialogView.findViewById(R.id.tv_nama);
        tv_email = (EditText) dialogView.findViewById(R.id.tv_email);
        tv_komentar = (EditText) dialogView.findViewById(R.id.tv_komentar);
        swipe = (SwipeRefreshLayout) dialogView.findViewById(R.id.swipeToRefresh);
        listkomen = (ListView) dialogView.findViewById(R.id.listkomen);

        adapter = new AdapterKomentar(EventDetail.this, komensList);
        listkomen.setAdapter(adapter);


        swipe.setRefreshing(true);
        swipe.setOnRefreshListener(this);

        komensList.clear();
        loadKomen(0);
        adapter.notifyDataSetChanged();

        //txt_id      = (EditText) dialogView.findViewById(R.id.txt_id);
        //txt_nama    = (EditText) dialogView.findViewById(R.id.txt_nama);
        //txt_alamat  = (EditText) dialogView.findViewById(R.id.txt_alamat);

        /*if (!idx.isEmpty()){
            txt_id.setText(idx);
            txt_nama.setText(namax);
            txt_alamat.setText(alamatx);
        } else {
            kosong();
        }*/

        dialog_komen.setPositiveButton(button, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                /*id      = txt_id.getText().toString();
                nama    = txt_nama.getText().toString();
                alamat  = txt_alamat.getText().toString();

                simpan_update();*/
                simpan();
            }
        });

        dialog_komen.setNegativeButton("BATAL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //kosong();
            }
        });

        listkomen.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }

            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                    swipe.setRefreshing(true);

                    handler = new Handler();
                    runnable = new Runnable() {
                        public void run() {
                            loadKomen(offSet);
                            //txt_alert.setVisibility(View.GONE);
                        }
                    };
                    handler.postDelayed(runnable, 1000);

                }
            }

        });

        dialog_komen.show();
    }

    // fungsi untuk menyimpan atau update
    private void simpan() {
        String url;
        // jika id kosong maka simpan, jika id ada nilainya maka update
        url = Server.url_insert_komen;

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    successs = jObj.getInt(TAG_SUCCESS);

                    // Cek error node pada json
                    if (successs == 1) {
                        Log.d("Add/update", jObj.toString());

                        komensList.clear();
                        adapter.notifyDataSetChanged();
                        loadKomen(0);
                        //callVolley();
                        //kosong();

                        Toast.makeText(EventDetail.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                        adapter.notifyDataSetChanged();

                    } else {
                        Toast.makeText(EventDetail.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(EventDetail.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                // jika id kosong maka simpan, jika id ada nilainya maka update
                params.put("id_relasi", id_event);
                params.put("nama", tv_nama.getText().toString());
                params.put("email", tv_email.getText().toString());
                params.put("komen", tv_komentar.getText().toString());
                params.put("jenis_komentar", jenis_komentar);

                return params;
            }

        };

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void loadKomen(final int page){

        swipe.setRefreshing(true);

        // Creating volley request obj
        final JsonArrayRequest arrReq = new JsonArrayRequest(url_list + page,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    KomentarData komen = new KomentarData();

                                    no = obj.getInt(TAG_NO);

                                    komen.setNama(obj.getString(TAG_NAME));
                                    komen.setEmail(obj.getString(TAG_EMAIL));

                                    komen.setKomen(obj.getString(TAG_KOMEN));
                                    //spot.setJenis_tempat(obj.getString(TAG_JENIS));

                                    // adding news to news array
                                    komensList.add(komen);

                                    if (no > offSet) {
                                        offSet = no;
                                    }

                                    Log.d(TAG, "offSet " + offSet);

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }

                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                adapter.notifyDataSetChanged();
                            }
                        }
                        swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                swipe.setRefreshing(false);
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);

    }

}
