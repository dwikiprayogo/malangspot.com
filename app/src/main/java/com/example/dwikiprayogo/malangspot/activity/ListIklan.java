package com.example.dwikiprayogo.malangspot.activity;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.fragments.HomeFragment;

public class ListIklan extends AppCompatActivity {

    Toolbar toolbar;
    EditText etsearch;
    TextView txt_kategori;
    ImageView img_x;
    boolean value_anim=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_iklan);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ListIklan.this);
        txt_kategori = (TextView) findViewById(R.id.kategori_name);

        etsearch = (EditText) findViewById(R.id.edtKeyword);
        img_x = (ImageView) findViewById(R.id.xcross_img);
        img_x.setVisibility(View.GONE);

        toolbar = (Toolbar) findViewById(R.id.toolbar_event);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        etsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (value_anim==true){
                    TranslateAnimation animate = new TranslateAnimation(img_x.getWidth(),0,0,0);
                    animate.setDuration(500);
                    animate.setFillAfter(true);
                    img_x.startAnimation(animate);
                    img_x.setVisibility(View.VISIBLE);
                    value_anim=false;
                }else{
                    //do nothing
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        img_x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etsearch.getText().clear();
                TranslateAnimation animate = new TranslateAnimation(0,img_x.getWidth(),0,0);
                animate.setDuration(500);
                animate.setFillAfter(true);
                img_x.startAnimation(animate);
                img_x.setVisibility(View.GONE);
                value_anim=true;
            }
        });

        String kategori = sharedPreferences.getString(HomeFragment.kategori_artikel, null);
        txt_kategori.setText(kategori);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
