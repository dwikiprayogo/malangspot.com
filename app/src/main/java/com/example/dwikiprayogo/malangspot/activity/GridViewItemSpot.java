package com.example.dwikiprayogo.malangspot.activity;

import android.graphics.drawable.Drawable;

/**
 * Created by DWIKI PRAYOGO on 13/03/2017.
 */

public class GridViewItemSpot {

    String title;
    Drawable image;

    // Empty Constructor
    public GridViewItemSpot()
    {

    }

    // Constructor
    public GridViewItemSpot(String title, Drawable image)
    {
        super();
        this.title = title;
        this.image = image;
    }

    // Getter and Setter Method
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Drawable getImage()
    {
        return image;
    }

    public void setImage(Drawable image)
    {
        this.image = image;
    }
}
