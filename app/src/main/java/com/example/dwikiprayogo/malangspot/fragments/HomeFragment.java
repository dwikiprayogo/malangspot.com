package com.example.dwikiprayogo.malangspot.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.activity.AdapaterGridView;
import com.example.dwikiprayogo.malangspot.activity.GridViewItem;
import com.example.dwikiprayogo.malangspot.activity.ListArtikel;
import com.example.dwikiprayogo.malangspot.activity.ListEvent;
import com.example.dwikiprayogo.malangspot.activity.ListIklan;
import com.example.dwikiprayogo.malangspot.activity.ListTalent;
import com.example.dwikiprayogo.malangspot.utils.SharedPreference;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by DWIKI PRAYOGO on 09/03/2017.
 */

public class HomeFragment extends Fragment implements OnItemClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    public static final String artikel_pref_name = "ArtikelPref" ;
    public static final String kategori_artikel = "nameKey";

    String k_event,k_artikel,k_iklan,k_talent;
    //CoordinatorLayout coordinatorLayout;

    SharedPreference sf;
    SharedPreferences sharedpreferences;

    GridView gridview;
    boolean value=true;
    AdapaterGridView gridviewAdapter;
    ArrayList<GridViewItem> data = new ArrayList<GridViewItem>();

    SliderLayout sliderLayout;
    HashMap<String, Integer> Hash_file_maps;
    HashMap<String, Integer> Hash_file_maps2;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //sharedpreferences = this.getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        sf = new SharedPreference(this.getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        gridview = (GridView) v.findViewById(R.id.gridView1);
        //coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id.coordinatorLayout);
        gridview.setOnItemClickListener(this);

        //String event = "true";
        //sf.EventNotif(event);

        //HashMap<String, String> event_notif = sf.getDataNotifEvent();
        //k_event = event_notif.get(SharedPreference.KEY_EVENT);

        //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        //String k_event = sharedPreferences.getString(HomeFragment.Event, null);
        if(value==true && sf.isEventIn()==true && sf.isIklanIn()==true && sf.isArtikelIn()==true && sf.isTalentIn()==true){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
        }else if(value==true && sf.isEventIn()== false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==true){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            //setDataAdapter();
        }else if(value==true && sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
        }else if (value==true && sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
        }else if (value==true && sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
        }else if (value==true && sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
        }else if(value==true && sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
        }else if (value==true && sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
        }else if(value==true && sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
        }else if(value==true && sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
        }else if(value==true && sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
        }else if(value==true && sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
        }else if(value==true && sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
        }else if(value==true && sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
        }else if(value==true && sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
        }else if(value==true && sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
            data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
        }

        setDataAdapter();

        Hash_file_maps = new HashMap<>();
        Hash_file_maps2 = new HashMap<>();

        sliderLayout = (SliderLayout) v.findViewById(R.id.slider);

        Hash_file_maps.put("Alun - Alun Kota Malang", R.drawable.alun_alun_malang);
        Hash_file_maps.put("Alun - Alun Tugu Kota Malang", R.drawable.alun_alun_tugu);
        Hash_file_maps.put("Hutan Kota Malabar", R.drawable.hutan_kota_malabar);
        Hash_file_maps.put("Stadion Gajayana", R.drawable.stadion_gajayana);
        Hash_file_maps.put("Andromeda Cafe and Music", R.drawable.andromeda_cafe_and_music);
        Hash_file_maps.put("Bali Cafe", R.drawable.bali_cafe);
        Hash_file_maps.put("Taman Baca Bentoel", R.drawable.taman_baca_bentoel);
        Hash_file_maps.put("Hope Cafe", R.drawable.hope_cafe);
        Hash_file_maps.put("Toko Oen", R.drawable.toko_oen);

        Hash_file_maps2.put("Museum Brawijaya", R.drawable.museum_brawijaya);
        Hash_file_maps2.put("Wisma Tumapel", R.drawable.wisma_tumapel);
        Hash_file_maps2.put("Inggil", R.drawable.inggil);

        for(String name : Hash_file_maps.keySet()){

            TextSliderView textSliderView = new TextSliderView(getContext());
            textSliderView
                    .description(name)
                    .image(Hash_file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);
            sliderLayout.addSlider(textSliderView);
        }

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        //sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);
        sliderLayout.addOnPageChangeListener(this);

        return v;
    }

    // Set the Data Adapter
    private void setDataAdapter() {
        gridviewAdapter = new AdapaterGridView(getActivity(), R.layout.fragment_adapter_home, data);
        gridview.setAdapter(gridviewAdapter);
        value=false;
    }



    @Override
    public void onItemClick(final AdapterView<?> arg0, final View view, final int position, final long id) {

        if (position==0){

            String event = "false";
            sf.EventNotif(event);
            //startActivity(new Intent(getActivity(), ListEvent.class));
            //getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
            //getActivity().finish();
            if(sf.isEventIn()==true && sf.isIklanIn()==true && sf.isArtikelIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()== false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
                //setDataAdapter();
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }

            setDataAdapter();

            startActivity(new Intent(getActivity(), ListEvent.class));
            getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);

         //   startActivity(new Intent(getActivity(), ActivityMenuList.class));
          //  getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
        }
        else if (position==1){

            String artikel = "false";
            sf.ArtikelNotif(artikel);

            if(sf.isEventIn()==true && sf.isIklanIn()==true && sf.isArtikelIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()== false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
                //setDataAdapter();
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }

            setDataAdapter();
            artikel_menu(getActivity(),"");
        }
        else if (position==2){

            String iklan = "false";
            sf.IklanNotif(iklan);

            if(sf.isEventIn()==true && sf.isIklanIn()==true && sf.isArtikelIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()== false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
                //setDataAdapter();
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }

            setDataAdapter();
            //iklan_menu(getActivity(),"");

        }
        else if (position==3){
            String talent = "false";
            sf.TalentNotif(talent);

            if(sf.isEventIn()==true && sf.isIklanIn()==true && sf.isArtikelIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()== false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
                //setDataAdapter();
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==true){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==true && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==false && sf.isArtikelIn()==true && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if(sf.isEventIn()==true && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }else if (sf.isEventIn()==false && sf.isArtikelIn()==false && sf.isIklanIn()==false && sf.isTalentIn()==false){
                data.clear();
                data.add(new GridViewItem(getResources().getString(R.string.menu_event), getResources().getDrawable(R.drawable.ic_event), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_artikel), getResources().getDrawable(R.drawable.ic_artikel), getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_iklan), getResources().getDrawable(R.drawable.ic_voting),  getResources().getDrawable(R.drawable.ic_notify_gone)));
                data.add(new GridViewItem(getResources().getString(R.string.menu_talent),getResources().getDrawable(R.drawable.ic_talent),  getResources().getDrawable(R.drawable.ic_notify_gone)));
            }

            setDataAdapter();
            entertainment_menu(getActivity(),"");
        }

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        information();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //getRetainInstance();
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
        //getRetainInstance();

    }

    public void information(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Informasi");
        alertDialog.setMessage("Silahkan pilih menu spot untuk deskripsi lebih lanjut...");
        alertDialog.setPositiveButton("OKE", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //startActivity(new Intent(getActivity(), SpotFragment.class));
            }
        });

        alertDialog.create().show();
    }

    public void artikel_menu(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.menu_artikel);

        Button txt_semua = (Button) dialog.findViewById(R.id.txt_semua);
        Button txt_kesehatan = (Button) dialog.findViewById(R.id.txt_kesehatan);
        Button txt_tips = (Button) dialog.findViewById(R.id.txt_tips);
        Button txt_olahraga = (Button) dialog.findViewById(R.id.txt_olahraga);
        Button txt_liburan = (Button) dialog.findViewById(R.id.txt_liburan);
        Button txt_santai = (Button) dialog.findViewById(R.id.txt_santai);

        txt_semua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListArtikel.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "0");
                editor.commit();
                //nothing
            }
        });

        txt_kesehatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListArtikel.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "2");
                editor.commit();
                //nothing
            }
        });

        txt_tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListArtikel.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "1");
                editor.commit();
                //nothing
            }
        });

        txt_olahraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListArtikel.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "4");
                editor.commit();
                //nothing
            }
        });

        txt_liburan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListArtikel.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "7");
                editor.commit();
                //nothing
            }
        });

        txt_santai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListArtikel.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "8");
                editor.commit();
                //nothing
            }
        });

        dialog.show();
    }

    public void iklan_menu(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.menu_iklan);

        Button txt_semua = (Button) dialog.findViewById(R.id.txt_semua);
        Button txt_jasa = (Button) dialog.findViewById(R.id.txt_jasa);
        Button txt_jualbeli = (Button) dialog.findViewById(R.id.txt_jualbeli);
        Button txt_loker = (Button) dialog.findViewById(R.id.txt_loker);
        Button txt_lainnya = (Button) dialog.findViewById(R.id.txt_lainnya);

        txt_semua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListIklan.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "semua");
                editor.commit();
                //nothing
            }
        });

        txt_jasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListIklan.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "jasa");
                editor.commit();
                //nothing
            }
        });

        txt_jualbeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListIklan.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "jualbeli");
                editor.commit();
                //nothing
            }
        });

        txt_loker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListIklan.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "loker");
                editor.commit();
                //nothing
            }
        });

        txt_lainnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListIklan.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "lainnya");
                editor.commit();
                //nothing
            }
        });

        dialog.show();
    }

    public void entertainment_menu(Activity activity, String msg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.menu_entertainment);

        Button txt_semua = (Button) dialog.findViewById(R.id.txt_semua);
        Button txt_nyanyi = (Button) dialog.findViewById(R.id.txt_nyanyi);
        Button txt_model = (Button) dialog.findViewById(R.id.txt_model);

        txt_semua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListTalent.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "0");
                editor.commit();
                //nothing
            }
        });

        txt_nyanyi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListTalent.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "2");
                editor.commit();
                //nothing
            }
        });

        txt_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListTalent.class));
                getActivity().overridePendingTransition (R.anim.open_next, R.anim.close_next);
                sharedpreferences = getActivity().getSharedPreferences(artikel_pref_name, Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(kategori_artikel, "1");
                editor.commit();
                //nothing
            }
        });

        dialog.show();
    }


}
