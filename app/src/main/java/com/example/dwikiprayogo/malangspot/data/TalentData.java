package com.example.dwikiprayogo.malangspot.data;

import android.graphics.drawable.Drawable;

/**
 * Created by DWIKI PRAYOGO on 10/04/2017.
 */

public class TalentData {

    private String id_talent, nama_talent, jenis_talent, gambar_talent;

    public TalentData() {
    }

    public TalentData(String id_talent, String nama_talent, String jenis_talent, String gambar_talent) {
        this.id_talent = id_talent;
        this.nama_talent = nama_talent;
        this.jenis_talent = jenis_talent;
        //this.alamat = alamat;
        this.gambar_talent = gambar_talent;
    }

    public String getId_talent() {
        return id_talent;
    }

    public void setId_talent(String id_talent) {
        this.id_talent = id_talent;
    }

    public String getJenis_talent() {
        return jenis_talent;
    }

    public void setJenis_talent(String jenis_talent) {
        this.jenis_talent = jenis_talent;
    }

    public String getNama_talent() {
        return nama_talent;
    }

    public void setNama_talent(String nama_talent) {
        this.nama_talent = nama_talent;
    }

    public String getGambar_talent() {
        return gambar_talent;
    }

    public void setGambar_talent(String gambar_talent) {
        this.gambar_talent = gambar_talent;
    }

}
