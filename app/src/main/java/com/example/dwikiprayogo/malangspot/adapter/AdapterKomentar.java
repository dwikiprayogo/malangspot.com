package com.example.dwikiprayogo.malangspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.KomentarData;
import com.example.dwikiprayogo.malangspot.data.VideoData;

import java.util.List;

/**
 * Created by DWIKI PRAYOGO on 26/04/2017.
 */

public class AdapterKomentar extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<KomentarData> komensItems;
    //ImageLoader imageLoader;

    public AdapterKomentar(Activity act, List<KomentarData> komensItems) {
        this.activity = act;
        this.komensItems = komensItems;
        //imageLoader = new ImageLoader(act);
    }

    @Override
    public int getCount() {
        return komensItems.size();
    }

    @Override
    public Object getItem(int location) {
        return komensItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.custom_list_komen, null);

        //TextView txtNo = (TextView) convertView.findViewById(R.id.txt_no);
        TextView txt_nama = (TextView) convertView.findViewById(R.id.txt_nama);
        TextView txt_email = (TextView) convertView.findViewById(R.id.txt_email);
        TextView txt_komen = (TextView) convertView.findViewById(R.id.txt_komen);
        //TextView txtKategori = (TextView) convertView.findViewById(R.id.txt_deskripsi);
        //TextView txtAlamat = (TextView)  convertView.findViewById(R.id.txt_alamat);
        //NetworkImageView thumbNail = (NetworkImageView) convertView.findViewById(R.id.video_gambar);

        KomentarData komens = komensItems.get(position);

        //thumbNail.setImageUrl("http://img.youtube.com/vi/"+videos.getUrl_video()+"/0.jpg", imageLoader);
        txt_nama.setText(komens.getNama());
        txt_email.setText(komens.getEmail());
        txt_komen.setText(komens.getKomen());
        //txtNo.setText(spots.getNo());
        //txtTextJudul.setText(spots.getNama_tempat());
        //txtKategori.setText(Html.fromHtml(spots.getDeskripsi()));
        //txtAlamat.setText(Html.fromHtml(spots.getAlamat()));

        return convertView;
    }
}
