package com.example.dwikiprayogo.malangspot.activity;

/**
 * Created by Kuhaku on 15/03/2017.
 */

public class AdapterListSpot {
    private String id_tempat;
    private String jenis_tempat;
    private String nama_tempat;
    private String alamat;
    private String deskripsi;
    private String latitude;
    private String longitude;
    private String gambar;
    //private String gambar;
    public AdapterListSpot(){
        this.id_tempat=id_tempat;
        this.jenis_tempat=jenis_tempat;
        this.nama_tempat= nama_tempat;
        this.alamat= alamat;
        this.deskripsi=deskripsi;
        this.latitude=latitude;
        this.longitude=longitude;
        this.gambar=gambar;
    }
    public String getId_tempat(){
        return id_tempat;
    }
    public void setId_tempat(String id_tempat){
        this.id_tempat=id_tempat;
    }

    public String getJenis_tempat(){
        return jenis_tempat;
    }
    public void setJenis_tempat(String jenis_tempat) {
        this.jenis_tempat = jenis_tempat;
    }

    public String getNama_tempat(){
        return nama_tempat;
    }
    public void setNama_tempat(String nama_tempat){
        this.nama_tempat = nama_tempat;
    }

    public String getAlamat(){
        return alamat;
    }
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDeskripsi(){
        return deskripsi;
    }
    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getLatitude(){
        return latitude;
    }
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude(){
        return longitude;
    }
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /*public String getGambar(){
        return gambar;
    }
    public  void setGambar(String gambar){
        this.gambar = gambar;
    }*/

}
