package com.example.dwikiprayogo.malangspot.data;

/**
 * Created by DWIKI PRAYOGO on 25/04/2017.
 */

public class VideoData {
    private String url_video, url;

    public VideoData() {
    }

    public VideoData(String url_video, String url) {
        this.url = url;
        this.url_video = url_video;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl_video() {
        return url_video;
    }

    public void setUrl_video(String url_video) {
        this.url_video = url_video;
    }
}
