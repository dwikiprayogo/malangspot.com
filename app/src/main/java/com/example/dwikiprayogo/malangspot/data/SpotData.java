package com.example.dwikiprayogo.malangspot.data;

/**
 * Created by Dwiki Prayogo on 29/02/2016.
 */
public class SpotData {
    private String no,id_tempat, jenis_tempat, nama_tempat, alamat, deskripsi,gambar;

    public SpotData() {
    }

    public SpotData(String no, String id_tempat, String jenis_tempat, String nama_tempat, String alamat, String deskripsi,String gambar) {
        this.no = no;
        this.id_tempat = id_tempat;
        this.jenis_tempat = jenis_tempat;
        this.nama_tempat = nama_tempat;
        this.alamat = alamat;
        this.deskripsi = deskripsi;
        this.gambar = gambar;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }


    public String getId_tempat() {
        return id_tempat;
    }

    public void setId_tempat(String id_tempat) {
        this.id_tempat = id_tempat;
    }

    public String getJenis_tempat() {
        return jenis_tempat;
    }

    public void setJenis_tempat(String jenis_tempat) {
        this.jenis_tempat = jenis_tempat;
    }

    public String getNama_tempat() {
        return nama_tempat;
    }

    public void setNama_tempat(String nama_tempat) {
        this.nama_tempat = nama_tempat;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

}
