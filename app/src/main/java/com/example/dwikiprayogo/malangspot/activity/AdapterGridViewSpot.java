package com.example.dwikiprayogo.malangspot.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dwikiprayogo.malangspot.R;

import java.util.ArrayList;

/**
 * Created by DWIKI PRAYOGO on 13/03/2017.
 */

public class AdapterGridViewSpot extends ArrayAdapter<GridViewItemSpot>{
    Context mContext;
    int resourceId;
    ArrayList<GridViewItemSpot> data = new ArrayList<GridViewItemSpot>();

    public AdapterGridViewSpot(Context context, int layoutResourceId, ArrayList<GridViewItemSpot> data) {
        super(context, layoutResourceId, data);
        this.mContext = context;
        this.resourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        AdapterGridViewSpot.ViewHolder holder = null;

        if (itemView == null) {
            final LayoutInflater layoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = layoutInflater.inflate(resourceId, parent, false);

            holder = new AdapterGridViewSpot.ViewHolder();
            holder.imgItem = (ImageView) itemView.findViewById(R.id.thumbnails);
            holder.txtItem = (TextView) itemView.findViewById(R.id.name);
            itemView.setTag(holder);
        } else {
            holder = (AdapterGridViewSpot.ViewHolder) itemView.getTag();
        }

        GridViewItemSpot item = getItem(position);
        holder.imgItem.setImageDrawable(item.getImage());
        holder.txtItem.setText(item.getTitle());

        return itemView;
    }

    static class ViewHolder {
        ImageView imgItem;
        TextView txtItem;
    }
}
