package com.example.dwikiprayogo.malangspot.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.activity.AdapaterGridView;
import com.example.dwikiprayogo.malangspot.activity.GridViewItem;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.SpotData;
import com.example.dwikiprayogo.malangspot.data.TalentData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DWIKI PRAYOGO on 10/04/2017.
 */

public class TalentAdapter extends ArrayAdapter<TalentData> {

    Context mContext;
    int resourceId, resourceIdN;
    ImageLoader imageLoader;
    ArrayList<TalentData> data_talent = new ArrayList<TalentData>();

    public TalentAdapter(Context context, int layoutResourceId, ArrayList<TalentData> data_talent) {
        super(context, layoutResourceId, data_talent);
        this.mContext = context;
        //this.resourceIdN = layoutResourceId;
        this.resourceId = layoutResourceId;
        this.data_talent = data_talent;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        ViewHolder holder = null;

        if (itemView == null) {
            final LayoutInflater layoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = layoutInflater.inflate(resourceId, parent, false);

            holder = new ViewHolder();
            holder.thumbNail = (NetworkImageView) itemView.findViewById(R.id.thumbnails_talent);
            holder.txtItemNama = (TextView) itemView.findViewById(R.id.name_talent);
            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) itemView.getTag();
        }

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        TalentData item = getItem(position);
        holder.thumbNail.setImageUrl(Server.url_image_talent+item.getGambar_talent(), imageLoader);
        holder.txtItemNama.setText(item.getNama_talent());

        return itemView;
    }

    static class ViewHolder {
        //ImageView imgItemNotif;
        NetworkImageView thumbNail;
        //ImageView imgItem;
        TextView txtItemNama;
    }
}
