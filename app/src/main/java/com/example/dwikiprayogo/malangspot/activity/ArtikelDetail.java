package com.example.dwikiprayogo.malangspot.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.dwikiprayogo.malangspot.R;
import com.example.dwikiprayogo.malangspot.Server;
import com.example.dwikiprayogo.malangspot.adapter.AdapterArtikelDet;
import com.example.dwikiprayogo.malangspot.adapter.SpotAdapter;
import com.example.dwikiprayogo.malangspot.app.AppController;
import com.example.dwikiprayogo.malangspot.data.ArtikelData;
import com.example.dwikiprayogo.malangspot.data.SpotData;
import com.example.dwikiprayogo.malangspot.utils.CustomListView;
import com.google.android.gms.maps.StreetViewPanoramaOptions;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArtikelDetail extends AppCompatActivity {

    Toolbar toolbar;

    private int offSet = 0;
    List<SpotData> spotsList = new ArrayList<SpotData>();
    AdapterArtikelDet adapter;

    int no, kategori_id;

    Integer id;
    Integer lenght_des;
    String judul,deskripsi,gambar,latitude,longitude,alamat;
    String[] data_gambar;
    long Spot_id;
    String Spot_kategori,Spot_nama,Spot_alamat,Spot_deskripsi,Spot_latitude,Spot_longitude,Spot_gambar;
    ListView lv_artikel;
    String url_detailArtikel,url_imageArtikel,url_footerArtikel, url_webview="http://malangspot.com/spot/jenis/Hotel";
    //ImageLoader imageLoader;
    TextView txt_namaArtikel,txt_tglArtikel,txtAlert, txt_deskripsiArtikel,txt_footer, txt_idArtikel, txtId_Artikel;
    ScrollView sv_detailArtikel;
    WebView wb_detail;
    Button btn_foto,btn_video,btn_lokasi,btn_rekom;
    ProgressBar prgLoading;
    int IOConnect = 0;
    ImageView img_detail;
    //CustomListViewLands customListViewLands;
    //Bundle item;

    ProgressDialog pDialog;
    JSONParser json;
    JSONParser jsonParser = new JSONParser();
    String success;
    JSONArray jsonArray;
    JSONObject jsonObject;
    List<NameValuePair> list;

    String[] post_url_image;
    Integer length_json=0;

    ViewPager vpImageArtikel;

    ImageAdapterArtikel sliderAdapterArtikel;

    private static final String TAG = ArtikelDetail.class.getSimpleName();

    public static final String TAG_NO 		    = "no";
    public static final String TAG_ID 		    = "id_artikel";
    public static final String TAG_NAMA 	= "nama_tempat";
    public static final String TAG_JUDUL	    = "judul_artikel";
    public static final String TAG_TGL 		    = "tgl_artikel";
    public static final String TAG_ID_SPOT 		= "id_tempat";
    public static final String TAG_JENIS_SPOT 	= "jenis_tempat";
    public static final String TAG_JENIS 	    = "jenis_artikel";
    public static final String TAG_DESKRIPSI 	= "deskripsi_artikel";
    public static final String TAG_RELASI 	    = "deskripsi";
    public static final String TAG_FOOTER 	    = "deskripsi_artikel";
    //public static final String TAG_DESKRIPSI= "deskripsi";

    //public static final String TAG_GAMBAR	= "gambar";

    //private static final String url_detail 	= Server.URL + "detail_news.php";
    String tag_json_obj = "json_obj_req";

    //private static String url = Server.link_image_detail_restaurant;

    Boolean cek_back=false;
    CustomListView customListView;
    String url_image, id_artikel, tgl_artikel, judul_artikel, url_list;

    Dialog dialog;
    FragmentManager fm;

    AlertDialog.Builder dialog_komen;
    LayoutInflater inflater;
    View dialogView;
    ImageView img;
    EditText tv_nama,tv_email,tv_komentar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artikel_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar_detailArtikel);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.nav_back2);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        id_artikel = getIntent().getStringExtra(TAG_ID);
        judul_artikel = getIntent().getStringExtra(TAG_JUDUL);
        tgl_artikel = getIntent().getStringExtra(TAG_TGL);
        txt_namaArtikel = (TextView) findViewById(R.id.txt_namaArtikel);
        txt_tglArtikel = (TextView) findViewById(R.id.txt_tglArtikel);
        lv_artikel = (ListView) findViewById(R.id.lv_artikel);
        txt_footer = (TextView) findViewById(R.id.txt_footer);
        wb_detail = (WebView) findViewById(R.id.detail_view);
        txt_deskripsiArtikel = (TextView) findViewById(R.id.txt_deskripsiArtikel);
        txt_namaArtikel.setText(""+judul_artikel);
        txt_tglArtikel.setText(""+tgl_artikel);
        vpImageArtikel = (ViewPager) findViewById(R.id.vpImageArtikel);

        //customListView.setExpanded(true);
        adapter = new AdapterArtikelDet(ArtikelDetail.this, spotsList);
        lv_artikel.setAdapter(adapter);


        url_imageArtikel = Server.url_image_artikel+"id_artikel="+id_artikel;
        url_list = Server.url_relasi_artikel+"?id_artikel=";
        url_detailArtikel = Server.url_detaildataArtikel;
        url_footerArtikel = Server.url_detailfooterArtikel;
        callHeaderArtikel(id_artikel);
        callFooterArtikel(id_artikel);
        //callRelasiArtikel(id_artikel);
        loadRelasi(id_artikel);
        new load_imageArtikel().execute();

        wb_detail.loadUrl(url_webview);

        lv_artikel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(ArtikelDetail.this, SpotDetail.class);
                //bundle.putString(TAG_ID, spotsList.get(position).getId_tempat());
                //bundle.putString(TAG_JENIS, spotsList.get(position).getJenis_tempat());
                //bundle.putString(TAG_NAMA, spotsList.get(position).getNama_tempat());
                //bundle.putString(TAG_ALAMAT, spotsList.get(position).getAlamat());
                //bundle.putString(TAG_GAMBAR, spotsList.get(position).getGambar());
                //bundle.putString("deskripsi", get_diskirpsi[position]);
                //bundle.putString("latitude",get_latitude[position]);
                //bundle.putString("longitude",get_longitude[position]);
                //intent.putExtras(bundle);
                intent.putExtra(TAG_ID_SPOT, spotsList.get(position).getId_tempat());
                intent.putExtra(TAG_JENIS_SPOT, spotsList.get(position).getJenis_tempat());
                startActivity(intent);
                overridePendingTransition(R.anim.open_next, R.anim.close_next);
            }
        });
    }

    private void callHeaderArtikel(final String id_artikel){
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading On Progress");
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        //swipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.POST, url_detailArtikel, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response " + response.toString());
                //swipe.setRefreshing(false);

                try {
                    JSONObject obj = new JSONObject(response);

                    String jenis_tempat    = obj.getString(TAG_JENIS);
                    String deskripsi_artikel      = obj.getString(TAG_DESKRIPSI);
                    //String Isi      = obj.getString(TAG_ISI);

                    //txt_judul.setText(nama_tempat);
                    //txt_idKategori.setText(id_tempat);
                    //txt_kategori.setText(jenis_tempat);
                    txt_deskripsiArtikel.setText(Html.fromHtml(deskripsi_artikel));
                    //wb_detail.loadData("<p style=\"text-align: justify\">" + item.getString("deskripsi") + "</p>", "text/html" , "UTF-8");
                    //txt_latitude.setText(latitude);
                    //txt_longitude.setText(longitude);
                    //txt_alamat.setText(alamat);

                    /*if (obj.getString(TAG_GAMBAR)!=""){
                        //thumb_image.setImageUrl(Gambar, imageLoader);
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Detail Spot Error: " + error.getMessage());
                Toast.makeText(ArtikelDetail.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //swipe.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_artikel", id_artikel);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        //linearLayout.setVisibility(View.VISIBLE);
        progressDialog.dismiss();
    }


    private void callFooterArtikel(final String id_artikel){
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading On Progress");
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        //swipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.POST, url_footerArtikel, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response " + response.toString());
                //swipe.setRefreshing(false);

                try {
                    JSONObject obj = new JSONObject(response);

                    //String jenis_tempat    = obj.getString(TAG_JENIS);
                    String deskripsi_artikel      = obj.getString(TAG_DESKRIPSI);
                    //String Isi      = obj.getString(TAG_ISI);

                    //txt_judul.setText(nama_tempat);
                    //txt_idKategori.setText(id_tempat);
                    //txt_kategori.setText(jenis_tempat);
                    txt_footer.setText(Html.fromHtml(deskripsi_artikel));
                    //wb_detail.loadData("<p style=\"text-align: justify\">" + item.getString("deskripsi") + "</p>", "text/html" , "UTF-8");
                    //txt_latitude.setText(latitude);
                    //txt_longitude.setText(longitude);
                    //txt_alamat.setText(alamat);

                    /*if (obj.getString(TAG_GAMBAR)!=""){
                        //thumb_image.setImageUrl(Gambar, imageLoader);
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Detail Artikel Error: " + error.getMessage());
                Toast.makeText(ArtikelDetail.this,
                        error.getMessage(), Toast.LENGTH_LONG).show();
                //swipe.setRefreshing(false);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_artikel", id_artikel);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        //linearLayout.setVisibility(View.VISIBLE);
        progressDialog.dismiss();
    }

    public class load_imageArtikel extends AsyncTask<String, String, String>
    {


        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ArtikelDetail.this);
            pDialog.setIndeterminate(false);

        }

        @Override
        protected String doInBackground(String... arg0) {


            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(url_imageArtikel);

            try {
                success = json.getString("success");

                Log.e("error", "nilai sukses=" + success);

                JSONArray hasil = json.getJSONArray("data_gambar");

                if (success.equals("1")) {

                    data_gambar = new String[hasil.length()];
                    for (int i = 0; i < hasil.length(); i++) {

                        JSONObject c = hasil.getJSONObject(i);
                        data_gambar[i] = c.getString("gambar".trim());
                        //session.get_data(jumlah_hit,jumlah_tim,jumlah_kal);
                        Log.e("ok", " ambil data");
                    }
                } else {
                    Log.e("error", "tidak bisa ambil data 0");
                }

            } catch (Exception e) {
                // TODO: handle exception
                Log.e("error", "tidak bisa ambil data 1");
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            if (success.equals("1")) {
                //linearLayout.setVisibility(View.VISIBLE);
                //Toast.makeText(getApplicationContext(), "Data sukses masuk broo ...", Toast.LENGTH_SHORT).show();
                sliderAdapterArtikel = new ImageAdapterArtikel(ArtikelDetail.this,data_gambar);
                vpImageArtikel.setAdapter(sliderAdapterArtikel);
            } else {
                Toast.makeText(getApplicationContext(), "Sorry, failed to response ...", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void callRelasiArtikel(final String id_artikel){
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading On Progress");
        progressDialog.setIndeterminate(false);
        progressDialog.show();

        //swipe.setRefreshing(true);

        StringRequest strReq = new StringRequest(Request.Method.POST, url_list, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response " + response.toString());

                if (response.length() > 0) {
                    // Parsing json
                    for (int i = 0; i < response.length(); i++) {
                        try {

                            JSONObject obj = new JSONObject(response);
                            SpotData spot = new SpotData();

                            no = obj.getInt(TAG_NO);
                            spot.setNo(obj.getString(TAG_NO));

                            //artikel.setId_artikel(obj.getString(TAG_ID));
                            spot.setNama_tempat(obj.getString(TAG_NAMA));

                            //if (obj.getString(TAG_GAMBAR) != "") {
                            //    artikel.setGambar_artikel(obj.getString(TAG_GAMBAR));
                            // }

                            //artikel.setTanggal_artikel(obj.getString(TAG_TGL));
                            spot.setDeskripsi(obj.getString(TAG_RELASI));

                            // adding news to news array
                            spotsList.add(spot);

                            //artikelsList.add(artikel);

                            //if (no > offSet)
                            //  offSet = no;

                            Log.d(TAG, "offSet " + offSet);

                        } catch (JSONException e) {
                            Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }
                //swipe.setRefreshing(false);
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //swipe.setRefreshing(false);
            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to post url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_artikel", id_artikel);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
        //linearLayout.setVisibility(View.VISIBLE);
        progressDialog.dismiss();
    }

    private void loadRelasi(String id_artikel){

        //swipe.setRefreshing(true);

        // Creating volley request obj

        JsonArrayRequest arrReq = new JsonArrayRequest(url_list + id_artikel, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "Response " + response.toString());

                        if (response.length() > 0) {
                            // Parsing json
                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    JSONObject obj = response.getJSONObject(i);
                                    SpotData spot = new SpotData();

                                    no = obj.getInt(TAG_NO);
                                    spot.setNo(obj.getString(TAG_NO));

                                    //artikel.setId_artikel(obj.getString(TAG_ID));
                                    spot.setId_tempat(obj.getString(TAG_ID_SPOT));
                                    spot.setJenis_tempat(obj.getString(TAG_JENIS_SPOT));
                                    spot.setNama_tempat(obj.getString(TAG_NAMA));

                                    //if (obj.getString(TAG_GAMBAR) != "") {
                                    //    artikel.setGambar_artikel(obj.getString(TAG_GAMBAR));
                                   // }

                                    //artikel.setTanggal_artikel(obj.getString(TAG_TGL));
                                    spot.setDeskripsi(obj.getString(TAG_RELASI));

                                    // adding news to news array
                                    spotsList.add(spot);

                                    //artikelsList.add(artikel);

                                    //if (no > offSet)
                                      //  offSet = no;

                                    Log.d(TAG, "offSet " + offSet);

                                } catch (JSONException e) {
                                    Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                                }

                                // notifying list adapter about data changes
                                // so that it renders the list view with updated data
                                adapter.notifyDataSetChanged();
                            }
                        }
                        //swipe.setRefreshing(false);
                    }

                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //swipe.setRefreshing(false);
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(arrReq);
        //txt_alert.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        //overridePendingTransition(R.anim.open_main, R.anim.close_next);
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
        //customListViewLands.imageLoader.clearCache();
        //clearData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                //customListViewLands.imageLoader.clearCache();
                //clearData();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
